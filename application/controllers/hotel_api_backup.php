<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
 * 
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Hotel_api extends REST_Controller
{
    /* API response POST request */
    function checkHotel_post()
    {
        date_default_timezone_set('Asia/Jakarta');
        // POST Request from Trip Advisor
		$ta_id = trim($this->post("ta_id")); // TripAdvisor hotel IDs (integer). Request by TripAdvisor
        $start_date = trim(date('d-M-Y',strtotime($this->post("start_date")))); // Start date (yyyy-mm-dd). Request by TripAdvisor
        $end_date = trim(date('d-M-Y',strtotime($this->post("end_date"))));  // end date (yyyy-mm-dd). Request by TripAdvisor
        $year_stay = trim(date('Y',strtotime($this->post("start_date"))));
        $month_stay = trim(date('m',strtotime($this->post("start_date"))));
        $day_stay = trim(date('d',strtotime($this->post("start_date"))));
        $num_adults = trim($this->post("num_adults")); // number adults (integer). Request by TripAdvisor
        $key = base64_decode(trim($this->post("query_key")));

        // count interval start date - end date
        $datetime1 = new DateTime($start_date);
        $datetime2 = new DateTime($end_date);
        $interval = $datetime1->diff($datetime2);
        $count = $interval->format('%a');

        $num_hotels = 1; // number hotel that had availabilty
        $hotels=$room_types="";
        $tax_val = $fee_val = 0;

        // this is query
        $this->db->select("YAD_NO,TAID,YAD_NAME,ROOM_TYPE_NAME,ADULT_SAL_PRICE_1,stay_day,service_charge,SUPPLY_RM_CNT,RSV_RM_CNT,lrg_name");
        $this->db->limit(3,0);
        $this->db->where("TAID",$ta_id);
       // $this->db->where("J_ROOM_DAY_STOCK.BH_FLG",1);
        $this->db->where("J_ROOM_DAY_STOCK.STAY_DAY >=",$start_date);
        $this->db->where("J_ROOM_DAY_STOCK.STAY_DAY <=",$end_date);
        $this->db->where("J_ROOM_PLAN_DAY_PRICE.STAY_DAY",$start_date);
        $this->db->where("J_ROOM_DAY_STOCK.SUPPLY_RM_CNT > RSV_RM_CNT");
        //$this->db->where("J_ROOM_DAY_STOCK.RSV_RM_CNT < J_ROOM_DAY_STOCK.SUPPLY_RM_CT");
        $this->db->join("USR_JIDSRV01.j_tripadvisor_hotel","USR_JIDSRV01.j_tripadvisor_hotel.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->join("USR_JIDSRV01.J_ROOM_PLAN_DAY_PRICE","J_ROOM_PLAN_DAY_PRICE.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->join("USR_JIDSRV01.J_ROOM_TYPE","J_ROOM_TYPE.ROOM_TYPE_CD=J_ROOM_PLAN_DAY_PRICE.ROOM_TYPE_CD");
        $this->db->join("USR_JIDSRV01.J_ROOM_DAY_STOCK","J_ROOM_DAY_STOCK.ROOM_TYPE_CD=J_ROOM_TYPE.ROOM_TYPE_CD");
        $this->db->join("USR_JIDSRV01.j_yad_area_map","j_yad_area_map.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->join("USR_JIDSRV01.j_sml_area_out","j_sml_area_out.SML_CD=j_yad_area_map.SML_CD");
        $this->db->join("USR_JIDSRV01.j_LRG_AREA_OUT_trans","j_LRG_AREA_OUT_trans.LRG_CD=j_yad_area_map.LRG_CD");
        $this->db->order_by("J_ROOM_PLAN_DAY_PRICE.ADULT_SAL_PRICE_1","asc");
        $hotels = $this->db->get("USR_JIDSRV01.J_YAD_KHN");
       // echo $this->db->last_query();
//        echo "<pre>";
//        print_r($hotels);
//        echo "</pre>";
//        die();


        if($key=='pegipegikeyapi')
        {
            if($hotels->num_rows() > 0)
            {
                foreach ($hotels->result() as $hotel) {

                    // hotel name key
                    $hotel_name = str_replace("-","_",url_title(strtolower($hotel->YAD_NAME)));
                    $hotel_id = $hotel->YAD_NO;
                    $hotelNameKey = rawurlencode($hotel->YAD_NAME);
                    $lrg_name =  str_replace("-","_",url_title(strtolower($hotel->LRG_NAME))); 

                    // url detail to pegipegi.com
                    //$url = "http://www.pegipegi.com/hotel/".$lrg_name."/".$hotel_name."_".$hotel_id."/?stayYear=".$year_stay."&stayMonth=".$month_stay."&stayDay=".$day_stay."&stayCount=". $count."&hotelNameKey=".$hotelNameKey."&roomCrack=200000";
                     $url = "http://www.pegipegi.com/hotel/".$lrg_name."/".$hotel_name."_".$hotel_id."/?stayYear=".$year_stay."&stayMonth=".$month_stay."&stayDay=".$day_stay."&stayCount=". $count."&utm_source=alliance&utm_medium=tripadvisor&utm_campaign=".$hotel_id."";


                    //check the hotel id, if doesn't set in hotel list, return to error
                    if($this->_checkHotelList($hotel_id)==true)
                    {

                    // get tax and fee
//                    $taxFee = $this->_getTaxAndFee($hotel_id);
//                    foreach($taxFee as $tf)
//                    {
//                        $tax_val = $tf->TAX;
//                        $fee_val = $tf->serviceCharge;
//                    }

                    # tax
                    $tax = ( $hotel->ADULT_SAL_PRICE_1 * 10 )/100;
                    $tax = floor($tax);

                    # fee
                    $fee = ( $hotel->ADULT_SAL_PRICE_1 * $hotel->SERVICE_CHARGE )/100;
                    $fee = floor($fee);

                    # final price
                    $final_price = $hotel->ADULT_SAL_PRICE_1 + $tax +$fee;
                    $final_price = $this->pembulatan($final_price);

                    //echo $tax;
                    $detail = array(
                                "url" =>$url, // url hotel id
                                "price"=>number_format($hotel->ADULT_SAL_PRICE_1), // price
                                "fees"=>number_format($fee), // fees
                                "taxes"=>number_format($tax), // taxes
                                "final_price"=>$final_price, // final price
                                "currency"=>"IDR" // currency format/code

                    );

                    // room name list
                    $hotel->ROOM_TYPE_NAME;
                    $room_types[$hotel->ROOM_TYPE_NAME] = $detail;

                    }else{


                       // hotel list
                        $hotels = array("hotel_id"=>$ta_id,"room_types"=>$room_types);

                        // if hotel available, error is false
                        $errors = "";
                    }

                }

                   // hotel list
                    $hotels = array("hotel_id"=>$ta_id,"room_types"=>$room_types);

                // if hotel available, error is false
                $errors = "";
            }
            else
            {
                // if hotel is not available, error message will showed

                $hotels = array();

                $errors = array("error_code"=>3,
                                "message"=>"Hotel code ".$ta_id." is no longer used.",
                                "timeout"=>600,
                                "hotel_ids"=>$ta_id);
            }

            // parsing to json
            $messages["api_version"] = 2;
            $messages["hotel_ids"] = array($ta_id);
            $messages["start_date"] = date('Y-m-d',strtotime($start_date));
            $messages["end_date"] = date('Y-m-d',strtotime($end_date));
            $messages["lang"] = "id_ID";
            $messages["num_adults"] = $num_adults;
            $messages["num_hotels"] = $num_hotels;
            $messages["hotels"] = $hotels;
            $messages["errors"] = $errors;
        }
        else
        {
            // parsing to json
            $messages["errors"] = "Invalid key. Could not connect to the API";
        }

        // show response
        $this->response($messages, 200); // 200 being the HTTP response code
    }


    /* Just for example API response. GET request */
    function checkHotelGet_get()
    {

        $ta_id = $this->get("ta_id");
        $start_date = $this->get("start_date");
        $end_date = $this->get("end_date");
        $num_adults = $this->get("num_adults");
        $num_hotels = 1;
        $hotels=array();

        //check available hotel in database
        $this->db->where('ta_id',$ta_id);
        $query = $this->db->get("hotellist");
        if($query->num_rows() > 0)
        {

            $url = base_url()."testform/detail/".$ta_id;
            $detail = array(
                        "url" =>$url,
                        "price"=>"1000",
                        "fees"=>"101",
                        "taxes"=>"5",
                        "final_price"=>"1106",
                        "currency"=>"IDR"

            );
            $room_types = array("Fenway Room"=>$detail);
            $hotels = array("hotel_id"=>$ta_id,"room_types"=>$room_types);

            $errors = "";
        }
        else
        {
            $errors = array("error_code"=>3,
                            "message"=>"Hotel code ".$ta_id." is no longer used.",
                            "timeout"=>600,
                            "hotel_ids"=>$ta_id);
        }

        $messages["api_version"] = 2; 
        $messages["hotel_ids"] = array($ta_id);
        $messages["start_date"] = $start_date; 
        $messages["end_date"] = $end_date; 
        $messages["lang"] = "id_ID";
        $messages["num_adults"] = $num_adults;
        $messages["num_hotels"] = $num_hotels;
        $messages["hotels"] = $hotels;
        $messages["errors"] = $errors;
        $this->response($messages, 200); // 200 being the HTTP response code
    }

    private function _getTaxAndFee($yad_no)
    {
        $taxAndfee = $this->db->query("select j_value_add_tax_mst.value_add_tax as tax,
        NVL(j_out_yad_ctl.tax_rate,
        j_sml_area_out.service_charge) as serviceCharge,
        j_out_yad_ctl.sys_use_rate as comission
        from j_yad_area_map, j_value_add_tax_mst, j_out_yad_ctl, j_sml_area_out
        where j_out_yad_ctl.yad_no = ".$yad_no."
        ");

        return $taxAndfee->result();
    }

    private function pembulatan($price)
    {
        $ratusan = substr($price, -3);
        if($ratusan<500)$akhir = $price - $ratusan;
        else$akhir = $price + (1000-$ratusan);
        return number_format($akhir, 0, ',', ',');
    }

      private function _checkHotelList($hotel_id)
    {
        //hotel list ID 12/11/2013 Array
        $hotels=array(
            //986427",
            "925002",
            "946868",
            "938430",
            "962611",
            "987252",
            "907588",
            "927832",
            "926120",
            "905322",
            "912933",
            "974315",
            "941222",
            "905885",
            "902677",
            "975432",
            "927616",
            "924800",
            "942073",
            "992855",
            "919040",
            "907108",
            "927960",
            "903868",
            "947884",
            "919298",
            "988404",
            "903388",
            "949000",
            "964301",
            "930037",
            "908073",
            "971310",
            "958994",
            "975837",
            "911740",
            "953280",
            "929864",
            "991361",
            "948556",
            "927020",
            "900518",
            "904519",
            "934678",
            "947330",
            "944746",
            "976988",
            "903390",
            "912609",
            "978982",
            "985992",
            "980572",
            "992062",
            "928730",
            "916946",
            "919777",
            "973215",
            "979399",
            "975130",
            "902136",
            "955276",
            "981476",
            "970800",
            "904077",
            "905674",
            "907300",
            "994545",
            "966288",
            "936901",
            "954858",
            "978634",
            "922912",
            "963648",
            "937650",
            "954120",
            "923214",
            "979842",
            "940173",
            "978128",
            "939422",
            "911761",
            "913120",
            "911175",
            "901355",
            "965402",
            "997564",
            "983760",
            "929128",
            "913297",
            "906582",
            "998884",
            "957792",
            "911780",
            "951296",
            "952620",
            "966570",
            "943712",
            "958816",
            "971621",
            "957742",
            "929672",
            "962464",
            "992100",
            "958708",
            "933392",
            "972195",
            "923394",
            "974640",
            "916563",
            "949496",
            "906056",
            "914268",
            "941248",
            "969640",
            "963364",
            "983146",
            "906790",
            "913708",
            "918656",
            "994968",
            "978086",
            "912380",
            "971162",
            "997894",
            "942946",
            "955573",
            "977420",
            "998068",
            "916178",
            "943346",
            "975837",
            "960752",
            "965936",
            "936072",
            "972984",
            "900745",
            "904490",
            "973728",
            "932728",
            "943800",
            "930379",
            "959782",
            "964085",
            "934576",
            "978740",
            "957651",
            "913213",
            "925258",
            "947851",
            "973596",
            "902030",
            "958720",
            "996886",
            "916881",
            "954241",
            "981405",
            "946868",
            "928770",
            "991004",
            "967340",
            "967320",
            "980950",
            "977392",
            "991612",
            "950345",
            "938536",
            "955672",
            "904717",
            "937415",
            "966090",
            "987718",
            "982096",
            "941007",
            "923740",
            "925180",
            "919486",
            "991003",
            "993932",
            "923613",
            "999782",
            "955665",
            "918515",
            "914052",
            "944150",
            "935820",
            "951050",
            "980418",
            "912680",
            "903582",
            "936752",
            "964048",
            "952872",
            "904120",
            "905072",
            "916258",
            "989127",
            "989126",
            "972565",
            "958917",
            "981161",
            "911595",
            "946946",
            "977256",
            "932150",
            "998558",
            "918048",
            "919480",
            "984392",
            "954742",
            "976005",
            "918720",
            "957154",
            "963346",
            "991376",
            "924954",
            "921112",
            "950268",
            "954455",
            "989708",
            "930264",
            "902145",
            "940230",
            "928976",
            "996468",
            "955792",
            "958398",
            "967370",
            "930379",
            "913240",
            "972614",
            "905697",
            "975167",
            "976374",
            "946372",
            "969918",
            "937130",
            "957960",
            "971592",
            "920976",
            "944574",
            "997120",
            "918878",
            "947816",
            "915051",
            "967508",
            "947637",
            "985092",
            "976936",
            "968175",
            "940230",
            "975624",
            "978027",
            "901072",
            "992911",
            "986248",
            "998292",
            "910110",
            "926584",
            "920740",
            "999726",
            "931390",
            "965806",
            "900520",
            "974848",
            "969660",
            "983627",
            "980328",
            "988668",
            "936838",
            "906962",
            "912962",
            "976115",
            "902947",
            "908416",
            "920469",
            "981672",
            "962881",
            "990396",
            "960346",
            "934800",
            "911110",
            "900108",
            "910060",
            "923365",
            "943486",
            "999676",
            "988092",
            "914044",
            "910687",
            "952092",
            "979320",
            "936662",
            "938936",
            "983627",
            "956499",
            "927831",
            "912592",
            "985973",
            "903423",
            "994676",
            "931721",
            "937415",
            "995510",
            "999205",
            "937589",
            "962144",
            "979429",
            "907588",
            "912706",
            "971608",
            "927352",
            "925361",
            "983035",
            "978884",
            "966244",
            "904320",
            "934845",
            "927066",
            "951172",
            "962611",
            "995530",
            "922672",
            "981556",
            "977591",
            "978755",
            "960744",
            "952120",
            "974992",
            "979298",
            "970925",
            "954458",
            "943060",
            "931932",
            "945112",
            "915608",
            "956716",
            "941386",
            "903883",
            "968307",
            "939853",
            "972900",
            "927670",
            "901584",
            "989199",
            "989846",
            "977022",
            "988523",
            "900415",
            "914070",
            "967728",
            "926608",
            "989365",
            "954536",
            "997365",
            "906032",
            "949584",
            "922544",
            "949765",
            "928304",
            "918472",
            "996207",
            "925280",
            "944835",
            "927370",
            "931116",
            "984456",
            "947788",
            "983384",
            "900830",
            "974196",
            "924307",
            "939853",
            "999806",
            "930920",
            "946148",
            "990188",
            "933720",
            "944588",
            "931720",
            "990910",
            "998530",
            "933070",
            "901292",
            "903681",
            "904070",
            "906264",
            "909236",
            "910105",
            "912577",
            "912644",
            "912855",
            "914314",
            "915546",
            "916414",
            "916984",
            "917645",
            "918771",
            "920816",
            "921491",
            "921508",
            "922772",
            "923261",
            "923783",
            "925024",
            "925232",
            "925648",
            "926324",
            "926910",
            "929288",
            "932600",
            "937715",
            "938725",
            "939352",
            "940396",
            "942638",
            "945140",
            "945800",
            "948144",
            "948477",
            "951394",
            "954132",
            "955913",
            "956452",
            "956907",
            "963716",
            "964240",
            "966646",
            "967944",
            "978537",
            "978922",
            "980732",
            "980800",
            "980855",
            "982020",
            "982212",
            "983265",
            "986348",
            "987726",
            "988412",
            "988987",
            "989068",
            "990084",
            "991293",
            "992784",
            "995686",
            "998710",
        );

        if(in_array($hotel_id, $hotels))
        {
            return true;
        }else{
            return false;
        }

       

    }


}