<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Curl_sample extends CI_Controller {

	 public function __construct(){
        parent::__construct();
        $this->load->library('curl');
    }
	
	function get_ip_address() 
    {
	    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
	        if (array_key_exists($key, $_SERVER) === true){
	            foreach (explode(',', $_SERVER[$key]) as $ip){
	                $ip = trim($ip); // just to be safe

	                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
	                    return $ip;
	                }
	            }
	        }
	    }
    }

    function index()
    {
    	//cho $this->get_ip_address();

        echo "
        <style>
        	td,tr{
        		padding:5px;
        	}
        	table{
        		border-collapse: collapse;
        	}
        </style>
        <table border=1>
        	<tr>
        		<td><b>API </b></td><td>:</td><td><a href='http://www.pegipegi.com/tripadvisor/hotel_availability/'>http://www.pegipegi.com/tripadvisor/hotel_availability/</a></td>
        	</tr>
        	<tr>
        		<td><b>Method </b></td><td>:</td><td>POST</td>
        	</tr>
        	<tr>
        		<td><b>Response </b></td><td>:</td><td>Json</td>
        	</tr>
        	<tr>
        		<td><b>Read </b></td><td>:</td><td>Curl</td>
        	</tr>
        <table>
        <br><hr>
		<h1>Request Tripadvisor API POST</h1>
		<form action='".base_url()."curlresponse.php' method='POST'>
		Trip Advisor Hotel ID Array<br>
		<input type='text' name='ta_id[]' value='634509'><br>
		<input type='text' name='ta_id[]' value='586362'><br>
		<input type='text' name='ta_id[]' value=''><br> Request by Tripadvisor. <br>
		<small><i>format : array integer</i></small><br><br>
		Start Date<br><input type='text' name='start_date' value='2014-01-15'> Request by Tripadvisor<br>
		<small><i>ex:start date = 2014-01-15  ( format : yyyy-mm-dd )</i></small><br>
		<br>End Date<br><input type='text' name='end_date' value='2014-01-17'> Request by Tripadvisor<br>
		<small><i>ex:end date = 2014-01-17  ( format : yyyy-mm-dd )</i></small><br><br>
		Num Adults<br><input type='text' name='num_adults' value='1'> Request by Tripadvisor<br>
		<small><i>ex:num = 1  ( format : integer )</i></small><br><br>
		Key<br><input type='text' name='query_key' value='cGVnaXBlZ2lrZXlhcGk='> Request by Tripadvisor<br>
		<small><i>ex: cGVnaXBlZ2lrZXlhcGk=  (format : string ) </i></small><br><br>
		<input type='submit' value='send'>
		</form>
		";
    }

     function ci_curl()  
	 {  
		$this->load->library('curl');  

		$start_date 	= $this->input->post("start_date");
		$end_date		= $this->input->post("end_date");
        $num_adults 	= $this->input->post("end_date");
        $key 			= $this->input->post("query_key");
        $hotels_id 		= $this->input->post('ta_id');

		$this->curl->create('http://www.pegipegi.com/tripadvisor/hotel_availability');  
	  
		// Optional, delete this line if your API is open  
		//$this->curl->http_login($username, $password);  

		$this->curl->post(array(  
		    'start_date' => $start_date,  
		    'end_date' => $end_date,
		    'num_adults'=>$num_adults,
		    //'hotels_id'	=> $hotels_id  
		));  
		 
		 echo $this->curl->execute();

		$result = json_decode($this->curl->execute());  
		  
		if(isset($result->errors) && $result->errors == '')  
		{  
		    //echo 'User has been updated.'; 
		    echo $this->curl->execute(); 
		}  
		  
		else  
		{  
		    echo 'Something has gone wrong';  
		}  
	}  

	function testcurl()
	{
		return "a";
	}

	function native_curl()  
  	{  
		$start_date 	= $this->input->post("start_date");
		$end_date		= $this->input->post("end_date");
        $num_adults 	= $this->input->post("end_date");
        $key 			= $this->input->post("query_key");
        $hotels_id 		= $this->input->post('ta_id');

		// Alternative JSON version  
		// $url = 'http://twitter.com/statuses/update.json';  
		// Set up and execute the curl process  
		$curl_handle = curl_init();  
		// curl_setopt($curl_handle, CURLOPT_URL, 'http://www.pegipegi.com/tripadvisor/hotel_availability');  
		curl_setopt($curl_handle, CURLOPT_URL, 'http://www.pegipegi.com/');  
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);  
		//curl_setopt($curl_handle, CURLOPT_POST, 1);  
		// curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(  
		//     'start_date' => $start_date,  
		//     'end_date' => $end_date,
		//     'num_adults'=>$num_adults,
		//     'hotels_id'=> serialize($hotels_id)
		// ));  
		  

		$buffer = curl_exec($curl_handle);  

		echo "<pre>";
		print_r($buffer); 
		echo "</pre>";

		curl_close($curl_handle);  
		  
		//$result = json_decode($buffer); 



		//echo $result->api_version;
		  
		// if(isset($result->status) && $result->status == 'success')  
		// {  
		//     echo 'User has been updated.';  
		// }  
		  
		// else  
		// {  
		//     echo 'Something has gone wrong';  
		// }  
   } 
   
}
