<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class export extends CI_Controller {

	 public function __construct(){
        parent::__construct();
    }
	
    function getareamap()
    {
        
        $areamap = $this->db->get("USR_JIDSRV01.j_yad_area_map");
        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=area.xls');
        header("Pragma: ");
        header("Cache-Control: ");

        echo "<table border=1>
        <tr>
            <td>hotel id</td>
            <td>lrg cd</td>
            <td>sml cd</td>
        </tr>
        ";
        foreach($areamap->result() as $val)
        {

              echo "<tr>
            <td>".$val->YAD_NO."</td>
            <td>".$val->LRG_CD."</td>
            <td>".$val->SML_CD."</td>
            </tr>";


        }
        echo "<table>";

    }

    function gethotel()
    {
        
        $areamap = $this->db->get("USR_JIDSRV01.J_YAD_KHN");
        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=hotels.xls');
        header("Pragma: ");
        header("Cache-Control: ");

        echo "<table border=1>
        ";
        foreach($areamap->result() as $val)
        {

              echo "<tr>
            <td>".$val->YAD_NO."</td>
            <td>".$val->YAD_NAME."</td>
            </tr>";


        }
        echo "<table>";

    }

     function getareamapsmall()
    {
        
        $areamap = $this->db->get("USR_JIDSRV01.j_sml_area_out_trans");
        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=area_small.xls');
        header("Pragma: ");
        header("Cache-Control: ");

        echo "<table border=1>
        <tr>
            <td>sml cd</td>
            <td>sml name</td>
            <td>lrg cd</td>
        </tr>
        ";
        foreach($areamap->result() as $val)
        {

            echo "<tr>
            <td>".$val->SML_CD."</td>
            <td>".$val->SML_NAME."</td>
            <td>".$val->LRG_CD."</td>
            </tr>";

        }
        echo "<table>";

    }

    function getareamaplarge()
    {
        
        $areamap = $this->db->get("USR_JIDSRV01.J_LRG_AREA_OUT_TRANS");
        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=area_large.xls');
        header("Pragma: ");
        header("Cache-Control: ");

        echo "<table border=1>
        <tr>
            <td>lrg cd</td>
            <td>lrg name</td>
        </tr>
        ";
        foreach($areamap->result() as $val)
        {

            echo "<tr>
            <td>".$val->LRG_CD."</td>
            <td>".$val->LRG_NAME."</td>
            </tr>";

        }
        echo "<table>";

    }

    function gethotelbooking()
    {
        date_default_timezone_set('Asia/Jakarta');

        $this->db->select('YAD_NO,U_DATE,RSV_NO');
        $datestart = trim(date('d-M-y',strtotime("-1days")));
        $datenow = trim(date('d-M-y',strtotime("now")));
        

       // $this->db->where("(payment_method_cd in('ATM', 'KBCA') and bank_payment_fin_flg=1 and can_type=0) or (payment_method_cd not in('ATM', 'KBCA') and can_type=0)");
        $this->db->where("U_DATE >=",'27-JAN-2014');
        $this->db->where("U_DATE <=",'31-JAN-2014');
        $this->db->order_by('U_DATE','desc');
        $this->db->where('CAN_TIME',null);

        $areamap = $this->db->get("USR_JIDSRV01.j_rsv");
        echo $this->db->last_query();
        echo "<pre>";
        print_r($areamap);
        echo "</pre>";
        die();
        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=hotel_booking_'.date('d-M-y',strtotime('-1 day')).'.xls');
        header("Pragma: ");
        header("Cache-Control: ");

        echo "<table border=1>
        <tr>
            <td>RSV_NO</td>
            <td>hotel id</td>
            <td>u date</td>
        </tr>
        ";
        foreach($areamap->result_array() as $val)
        {

            echo "<tr>
            <td>".$val['RSV_NO']."</td>
            <td>".$val['YAD_NO']."</td>
            <td>".date('Y-m-d',strtotime($val['U_DATE']))."</td>
            </tr>";

        }
        echo "<table>";

    }



    function getarea()
    {
        $this->db->select("SML_CD,SML_NAME,J_LRG_AREA_OUT.LRG_CD,J_LRG_AREA_OUT.LRG_NAME");
        //$this->db->join("USR_JIDSRV01.J_LRG_AREA_OUT","J_LRG_AREA_OUT.LRG_CD=j_sml_area_out.LRG_CD");
        $this->db->order_by('SML_NAME','asc');
        $area = $this->db->get("USR_JIDSRV01.j_sml_area_out");

        // echo "<pre>";
        // print_r($area);
        // echo "</pre>";

        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=hotels_list.xls');
        header("Pragma: ");
        header("Cache-Control: ");

        echo "<table border=1>";

        date_default_timezone_set('Asia/Jakarta');

        foreach($hotels->result() as $val)
        {

              echo "<tr>
            <td>".$val->SML_CD."</td>
            <td>".$val->SML_NAME."</td>
            <td>".$val->LRG_CD."</td>
            </tr>";


        }

        echo "<table>";
    }

    public function excel2()
    {

        // this is query
        $this->db->select("YAD_NO,YAD_NAME,LOWER(YAD_NAME) as YAD_NAME_ORD,LRG_NAME,YAD_ADRS_ENG,LCL_YBN_NO");
        $this->db->join("USR_JIDSRV01.j_yad_area_map","j_yad_area_map.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->join("USR_JIDSRV01.j_LRG_AREA_OUT_trans","j_LRG_AREA_OUT_trans.LRG_CD=j_yad_area_map.LRG_CD");
        $this->db->join("USR_JIDSRV01.j_yad_ctl","USR_JIDSRV01.j_yad_ctl.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->where("j_yad_ctl.open_flg","1");
        $this->db->order_by('LOWER(YAD_NAME)','ASC');
        //$this->db->limit(2,0);
        $hotels = $this->db->get("USR_JIDSRV01.J_YAD_KHN");

        // echo "<pre>";
        // print_r($hotels);
        // echo "</pre>";
        // die();
        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=hotels_list.xls');
// Fix for crappy IE bug in download.
        header("Pragma: ");
        header("Cache-Control: ");

      
        echo "<table border=1><tr><th>Hotel ID</th>
        <th>Hotel Name</th>
        <th>Kota</th>
        <th>Address</th>
        <th>URL</th>
        <th>Zip Code</th>
        </tr>";

        date_default_timezone_set('Asia/Jakarta');

        foreach($hotels->result() as $val)
        {

        	 $hotel_name_url = str_replace("-","_",url_title(strtolower($val->YAD_NAME)));
             $hotel_id = $val->YAD_NO;
             $lrg_name =  str_replace("-","_",url_title(strtolower($val->LRG_NAME)));

        	$url =  "http://www.pegipegi.com/hotel/".$lrg_name."/".$hotel_name_url."_".$hotel_id."/";

              echo "<tr>
            <td>".$val->YAD_NO."</td>
            <td>".$val->YAD_NAME."</td>
            <td>".$val->LRG_NAME."</td>
            <td>".$val->YAD_ADRS_ENG."</td>
            <td><a href=".$url.">".$url."</a></td>
            <td>".$val->LCL_YBN_NO."</td>
            </tr>";


        }

        echo "<table>";

    }

     protected function _excel()
    {

        // this is query
        $this->db->select("J_YAD_KHN.YAD_NO,J_YAD_KHN.YAD_NAME,j_tripadvisor_hotel.TAID,j_yad_ctl.open_flg");
        $this->db->join("USR_JIDSRV01.j_tripadvisor_hotel","USR_JIDSRV01.j_tripadvisor_hotel.YAD_NO=J_YAD_KHN.YAD_NO","LEFT");
        $this->db->join("USR_JIDSRV01.j_yad_ctl","USR_JIDSRV01.j_yad_ctl.YAD_NO=J_YAD_KHN.YAD_NO");
        //$this->db->where("j_yad_ctl.open_flg","1");
        $this->db->order_by("USR_JIDSRV01.J_YAD_KHN.YAD_NAME","asc");
        $hotels = $this->db->get("USR_JIDSRV01.J_YAD_KHN");

        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=hotels_list.xls');
// Fix for crappy IE bug in download.
        header("Pragma: ");
        header("Cache-Control: ");

        //open
        $this->db->select("J_YAD_KHN.YAD_NO,J_YAD_KHN.YAD_NAME,j_tripadvisor_hotel.TAID,j_yad_ctl.open_flg");
        $this->db->join("USR_JIDSRV01.j_tripadvisor_hotel","USR_JIDSRV01.j_tripadvisor_hotel.YAD_NO=J_YAD_KHN.YAD_NO","LEFT");
        $this->db->join("USR_JIDSRV01.j_yad_ctl","USR_JIDSRV01.j_yad_ctl.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->where("j_yad_ctl.open_flg","1");
        $this->db->order_by("USR_JIDSRV01.J_YAD_KHN.YAD_NAME","asc");
        $open = $this->db->get("USR_JIDSRV01.J_YAD_KHN");

        //close
        $this->db->select("J_YAD_KHN.YAD_NO,J_YAD_KHN.YAD_NAME,j_tripadvisor_hotel.TAID,j_yad_ctl.open_flg");
        $this->db->join("USR_JIDSRV01.j_tripadvisor_hotel","USR_JIDSRV01.j_tripadvisor_hotel.YAD_NO=J_YAD_KHN.YAD_NO","LEFT");
        $this->db->join("USR_JIDSRV01.j_yad_ctl","USR_JIDSRV01.j_yad_ctl.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->where("j_yad_ctl.open_flg","0");
        $this->db->order_by("USR_JIDSRV01.J_YAD_KHN.YAD_NAME","asc");
        $close = $this->db->get("USR_JIDSRV01.J_YAD_KHN");


        echo "<b>Total :</b> ".$hotels->num_rows()."<br><b>Open:</b>".$open->num_rows()." <br><b>Closed : ".$close->num_rows()."</b>";

        echo "<table border=1><tr><th>Pegipegi.com Hotel ID</th>
        <th>Pegipegi.com Hotel Name</th>
        <th>Tripadvisor Hotel ID</th>
        <th>Status</th>
        </tr>";

        date_default_timezone_set('Asia/Jakarta');

        foreach($hotels->result() as $val)
        {

            $flag = $val->OPEN_FLG == 1 ? "<b style='color:green'>open</b>" : "<b style='color:red'>closed</b>";

            // echo "<tr>
            // <td>".$val->YAD_NO."</td>
            // <td>".$val->YAD_NAME."</td>
            // <td>".$val->TAID."</td>
            // <td>".$flag."</td>

            // </tr>";


              echo "<tr>
            <td>".$val->YAD_NO."</td>
            <td>".$val->YAD_NAME."</td>
            <td>".$val->TAID."</td>
            <td>".$flag."</td>

            </tr>";


        }

        echo "<table>";

    }

	public function excel3()
    {

//j_out_yad_ctl.offline_flg = 0  &  j_out_yad_ctl.open_flg=1

        // this is query
        $this->db->select("YAD_NO,YAD_NAME,LOWER(YAD_NAME) as YAD_NAME_ORD,LRG_NAME,SML_NAME,YAD_ADRS_ENG,LCL_YBN_NO");
        $this->db->join("USR_JIDSRV01.j_yad_area_map","j_yad_area_map.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->join("USR_JIDSRV01.j_LRG_AREA_OUT_trans","j_LRG_AREA_OUT_trans.LRG_CD=j_yad_area_map.LRG_CD");
        $this->db->join("USR_JIDSRV01.j_sml_area_out_trans","j_sml_area_out_trans.SML_CD=j_yad_area_map.SML_CD");
        $this->db->join("USR_JIDSRV01.j_yad_ctl","USR_JIDSRV01.j_yad_ctl.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->join("USR_JIDSRV01.j_out_yad_ctl","USR_JIDSRV01.j_out_yad_ctl.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->where("j_yad_ctl.open_flg","1");
        $this->db->where("j_out_yad_ctl.offline_flg","0");
        $this->db->order_by('LOWER(YAD_NAME)','ASC');
        $this->db->limit(5,0);
        $hotels = $this->db->get("USR_JIDSRV01.J_YAD_KHN");

        echo "<pre>";
        print_r($hotels);
        echo "</pre>";
        die();
        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=hotels_list_url.xls');
        // Fix for crappy IE bug in download.
        header("Pragma: ");
        header("Cache-Control: ");

      
        echo "
        <table border=1>
        <tr><th>Hotel ID</th>
        <th>Hotel Name</th>
        <th>Big Area</th>
        <th>Small Area</th>
        <th>Hotel URL</th>
        </tr>";

        date_default_timezone_set('Asia/Jakarta');

        foreach($hotels->result() as $val)
        {

             $hotel_name_url = str_replace("-","_",url_title(strtolower($val->YAD_NAME)));
             $hotel_id = $val->YAD_NO;
             $lrg_name =  str_replace("-","_",url_title(strtolower($val->LRG_NAME)));

            $url =  "http://www.pegipegi.com/hotel/".$lrg_name."/".$hotel_name_url."_".$hotel_id."/";

              echo "<tr>
            <td>".$val->YAD_NO."</td>
            <td>".$val->YAD_NAME."</td>
            <td>".$val->LRG_NAME."</td>
            <td>".$val->SML_NAME."</td>
            <td><a href=".$url.">".$url."</a></td>
            </tr>";


        }

        echo "<table>";

    }

    function excel4()
    {
        $num_adults = 2;
        $start_date_db = "20140318";
        $end_date_db   = "20140328";

        $hotels = $this->db->query("SELECT
                    YCT.YAD_NO,
                    YKH.YAD_NAME,
                    OYC.SETT_CRCY_CD,
                    JAT.TAID
                     FROM J_YAD_CTL YCT 
                     INNER JOIN J_OUT_YAD_CTL OYC ON OYC.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_YAD_KHN YKH ON YKH.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_SEARCH_ROOM_PLAN SRP ON SRP.YAD_NO = YKH.YAD_NO 
                     INNER JOIN J_ROOM_PLAN_DAY_PRICE RPDP ON SRP.YAD_NO = RPDP.YAD_NO AND SRP.PLAN_CD = RPDP.PLAN_CD AND SRP.ROOM_TYPE_CD = RPDP.ROOM_TYPE_CD 
                     INNER JOIN J_ROOM_DAY_STOCK RDS ON RPDP.YAD_NO = RDS.YAD_NO AND RPDP.ROOM_TYPE_CD = RDS.ROOM_TYPE_CD AND RPDP.STAY_DAY = RDS.STAY_DAY 
                     INNER JOIN J_PLAN_SAL_CONTROL PSC ON RPDP.YAD_NO = PSC.YAD_NO AND RPDP.PLAN_CD = PSC.PLAN_CD AND RPDP.STAY_DAY = PSC.STAY_DAY 
                     INNER JOIN J_YAD_AREA_MAP YAM ON YAM.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_SML_AREA_OUT SAO ON SAO.SML_CD = YAM.SML_CD 
                     INNER JOIN j_LRG_AREA_OUT_trans LAO ON LAO.LRG_CD = YAM.LRG_CD 
                     INNER JOIN J_TIME_DIFF TD ON TD.LRG_CD = YAM.LRG_CD 
                     INNER JOIN J_EX_RATE ER ON ER.CRCY_CD = OYC.SETT_CRCY_CD
                     INNER JOIN USR_JIDSRV01.j_tripadvisor_hotel JAT ON JAT.YAD_NO = YCT.YAD_NO
                     INNER JOIN J_ROOM_TYPE JRT ON JRT.ROOM_TYPE_CD = RPDP.ROOM_TYPE_CD
                     LEFT OUTER JOIN J_TDK JTDK ON JTDK.YAD_NO = YCT.YAD_NO
                     WHERE YCT.REF_FLG = '1' 
                        AND (YKH.YAD_SYU_CD IS NULL OR YKH.YAD_SYU_CD <> '13') 
                        AND NOT(NVL(RDS.ARVL_STOP_FLG,0) = 1 
                        AND RDS.STAY_DAY = TO_DATE('$start_date_db','YYYYMMDD')) 
                        AND YCT.COUNTRY_FLG = '1' 
                        AND SRP.ROOM_CAPA_MIN_NUM <= to_number('$num_adults') 
                        AND SRP.ROOM_CAPA_MOST_NUM >= to_number('$num_adults') 
                        AND SRP.RSV_RCT_ENBL_FLG = '1' 
                        AND RPDP.STAY_DAY BETWEEN TO_DATE('$start_date_db','YYYYMMDD') 
                        AND TO_DATE('$end_date_db','YYYYMMDD') 
                        AND SRP.SPLY_PERIOD_STR_DAY <= TO_DATE('$end_date_db' ,'YYYYMMDD') 
                        AND SRP.SPLY_PERIOD_END_DAY >= TO_DATE('$start_date_db' ,'YYYYMMDD')  
                        AND( TO_DATE('$end_date_db' ,'YYYYMMDD') - SRP.RSV_END_DAY + round(SRP.RSV_END_HH / 100,0)/24 + mod(SRP.RSV_END_HH,100)/1440  >= TO_DATE('$start_date_db','YYYYMMDD') + TO_NUMBER('09')/24 + (TD.TIME_DIFF - 8)/24 + TO_NUMBER('43')/1440 ) 
                        AND RPDP.SAL_STAT_CD = '0' AND RDS.SAL_STAT_CD = '0' AND PSC.SAL_STAT_CD = '0' AND RDS.SUPPLY_RM_CNT - RDS.RSV_RM_CNT >= TO_NUMBER('1') 
                        AND ( RDS.LNG_STY_LIMIT_FLG <> '1'  OR RDS.SHORT_LNG_STY <= TO_NUMBER('1') ) 
                        AND SRP.SECRET_PLAN_FLG = '0' 
                        AND RPDP.ADULT_SAL_PRICE_1 is not NULL
                        GROUP BY YCT.YAD_NO,YKH.YAD_NAME,OYC.SETT_CRCY_CD,JAT.TAID
                        
                        ");
        // echo $hotels->num_rows();
        // echo "<pre>";
        // print_r($hotels->result());
        // echo "</pre>";
        // die();
        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=hotel_tripadvisor.xls');
        // Fix for crappy IE bug in download.
        header("Pragma: ");
        header("Cache-Control:");

      
        echo "
        <table border=1>
        <tr><th>Hotel ID</th>
        <th>TA ID</th>
        <th>Hotel Name</th>
        <th>Currency</th>
        </tr>";

        date_default_timezone_set('Asia/Jakarta');

        foreach($hotels->result() as $val)
        {

              echo "<tr>
            <td>".$val->YAD_NO."</td>
            <td>".$val->TAID."</td>
            <td>".$val->YAD_NAME."</td>
            <td>".$val->SETT_CRCY_CD."</td>
            </tr>";


        }

        echo "<table>";
    }


    function apitest()
    {
                $api_url       = "http://jws.pegipegi.com/rs/rsp0100/Rst0101Action.do?key=peg11187712b79&xml_ptn=2&reg=ID&ln=in&stay_date=20140320&count=1&start=1&adult_num=1&internal=1";

        $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL               , $api_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER    , 1);
                curl_setopt($ch, CURLOPT_HEADER             , 'Content-Type:application/xml');
                $xml_data   = curl_exec($ch);

                $xml_data   = str_replace('xmlns="jws"', '', $xml_data);

                $xml        = new SimpleXMLElement($xml_data);


                echo "<pre>";
                print_r($xml->Hotel);
                echo "</pre>";
                die();
    }

}
