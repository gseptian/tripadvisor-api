<?php defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package     CodeIgniter
 * @subpackage  Rest Server
 * @category    Controller
 * @author      Phil Sturgeon
 * @link        http://philsturgeon.co.uk/code/
 *
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Hotel_api_dev2 extends REST_Controller
{
    /* API response POST request */
    function checkHotel_post()
    {

        date_default_timezone_set('Asia/Jakarta');

        // POST Request from Trip Advisor
        //$ta_id = trim($this->post("ta_id")); // TripAdvisor hotel IDs (integer). Request by TripAdvisor
        $start_date = trim(date('d-M-Y',strtotime(isset($_POST["start_date"]) ? $_POST["start_date"] : " " ))); // Start date (yyyy-mm-dd). Request by TripAdvisor
        $end_date = trim(date('d-M-Y',strtotime(isset($_POST["end_date"]) ? $_POST["end_date"] : " " )));  // end date (yyyy-mm-dd). Request by TripAdvisor
        $year_stay = trim(date('Y',strtotime(isset($_POST["start_date"]) ? $_POST["start_date"] : " " )));
        $month_stay = trim(date('m',strtotime(isset($_POST["start_date"]) ? $_POST["start_date"] : " " )));
        $day_stay = trim(date('d',strtotime(isset($_POST["start_date"]) ? $_POST["start_date"] : " " )));
        $num_adults = trim(isset($_POST["num_adults"]) ? $_POST["num_adults"] : " " ); // number adults (integer). Request by TripAdvisor
        $key = base64_decode(trim(isset($_POST["query_key"]) ? $_POST["query_key"] : " " ));

        $lang = trim(isset($_POST["lang"]) ? $_POST["lang"] : "id_ID"  );

        $langApi ='en';
        if($lang=='id_ID')
        {
            $langApi = 'in';
        }

        // $lang = $lang == '' ? 'id_ID' : $lang;

        $d = $this->is_serialized(isset($_POST['hotels']) ? $_POST['hotels'] : " " );
        if($d==true)
        {
           $hotels_request = json_decode(unserialize(isset($_POST['hotels']) ? $_POST['hotels'] : " "));
        }else{
            $hotels_request = json_decode(isset($_POST['hotels']) ? $_POST['hotels'] : " ");
        }


        // echo "<pre>";
        // print_r($hotels_request);
        // echo "</pre>";
        // echo "<hr>";
        
        /* filter array hotel and ta ID by unique */
        $new_hotels_request = array ();
        foreach ($hotels_request as $row) 
            if (!in_array($row,$new_hotels_request)) array_push($new_hotels_request,$row);

       // print_r ($new_hotels_request);
        $hotels_request = $new_hotels_request;

        $dateNow = date('d-M-Y');

        $hotels_array=array();

        // count interval start date - end date
        $datetime1 = new DateTime($start_date);
        $datetime2 = new DateTime($end_date);
        $interval = $datetime1->diff($datetime2);
        $count = $interval->format('%a');

        $num_hotels = "";//1; // number hotel that had availabilty
        $hotels=$room_types=$start_date_db="";
        $tax_val = $fee_val = 0;

        $num_hotels = count($hotels_request);


        if($num_hotels<=0)
        {
            $messages["errors"] = "Hotel ID must be defined";
        }
        else
        {

            $no=0;
            $hotels_array=array();
            $errors = array();
            $error_count = 0;
            $ta_id_error = "";
            
            $start_date_db   = str_replace('-', '', $start_date);
            $start_date_target =  date('Ymd',strtotime($start_date_db));

            $start_date_db   = date('Ymd',strtotime($start_date));
            $end_date_db     = date('Ymd',strtotime($end_date));

            foreach($hotels_request as $value){

                $ta_id        = trim($value->ta_id);
                $partner_id   = trim($value->partner_id);
                $partner_url  = trim($value->partner_url);

                $ta_id_array[]=$ta_id; 

                // this is query
                $hotels = $this->db->query("SELECT YAD_NO FROM USR_JIDSRV01.j_tripadvisor_hotel
                        where TAID ='".$ta_id."' AND YAD_NO='".$partner_id."' ");

                if($end_date_db > $start_date_db)
                {
                        if($hotels->num_rows() > 0) {
                            
                                if(($hotels->num_rows() > 0) && ($num_adults <=9))
                                {

                                    
                                    $api_url        ="http://jws.pegipegi.com/rs/rsp0100/Rst0103Action.do?key=06b198209b3ffe&reg=ID&ln=".$langApi."&stay_date=".$start_date_target."&h_id=".$partner_id."&adult_num=".$num_adults."&stay_count=".$count."&room_count=1";
                                    //$api_url = "http://www.pegipegi.com/";
                                    $proxy      = '54.254.129.1:8888';
                                    $proxyauth  = 'pepe:password';
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
                                    curl_setopt($ch, CURLOPT_URL               , $api_url);
                                    curl_setopt($ch , CURLOPT_PROXY             , $proxy);
                                    curl_setopt($ch , CURLOPT_PROXYUSERPWD      , $proxyauth);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER    , 1);
                                    curl_setopt($ch, CURLOPT_HEADER, 0);
                                    curl_setopt($ch, CURLOPT_HEADER             , 'Content-Type:application/xml');
                                    $xml_data   = curl_exec($ch);
                                    

                                    if(!preg_match('/<LowPriceRoomPlan>/i', $xml_data)){

                                         $errors[] = array("error_code"=>1,
                                        "message"=>"Server maintenance",
                                        "timeout"=>600,
                                        "hotel_ids"=>array());

                                    }else{
                                        $xml_data   = str_replace('xmlns="jws"', '', $xml_data);
                                        $xml        = new SimpleXMLElement($xml_data);
                                       
                                        if($xml->NumberOfResults > 0)
                                        {
                                            $num_hotels_count=0;

                                                $hotel_id           = $partner_id;
                                                $HotelDetailURL     = $xml->Plan[0]->HotelDetailUrl;
                                                $room_name          = $xml->LowPriceRoomPlan->RoomPlanInfo->RoomName;
                                                $PlanName           = $xml->LowPriceRoomPlan->RoomPlanInfo->PlanName;
                                                $min_price          = $xml->LowPriceRoomPlan->PriceInfo->RoomPlanPrice * $count;
                                                $service_charge     = $xml->LowPriceRoomPlan->PriceInfo->ServiceCharge * $count;
                                                $tax                = $xml->LowPriceRoomPlan->PriceInfo->Tax * $count;
                                                $final_price        = $xml->LowPriceRoomPlan->ReservationInfo->PriceAfterTaxAndServiceCharge;
                                                $totalTaxAndServiceCharge = $xml->LowPriceRoomPlan->ReservationInfo->TotalTaxAndServiceCharge;

                                                $room_namep         = $room_name." - ".$PlanName;
                                                // url detail to pegipegi.com

                                                if($langApi=='en')
                                                {
                                                    $HotelDetailURL = str_replace("http://en.pegipegi.com/", "http://www.pegipegi.com/", $HotelDetailURL);
                                                }
                                                

                                                $url = $HotelDetailURL."?stayYear=".$year_stay."&stayMonth=".$month_stay."&stayDay=".$day_stay."&stayCount=". $count."&roomCrack=".$num_adults."00000&utm_source=alliance&utm_medium=tripadvisor&utm_campaign=".$hotel_id."";

                                

                                            if($this->_checkHotelList($hotel_id)==true)
                                            {
                                                $detail = array(
                                                    "url" =>$url, // url hotel id
                                                    "price"=>intval($min_price), // price
                                                    "fees"=>intval($totalTaxAndServiceCharge), // fees
                                                    //"taxes"=>intval($tax), // taxes
                                                    "taxes"=>"",
                                                    "final_price"=>intval($final_price), // final price
                                                    "currency"=>"IDR" // currency format/code
                                                );
                            
                                                $room_types = array( $room_namep => $detail);
                                                $hotels_array[] = array("hotel_id"=>intval($ta_id),'room_types'=>$room_types);
                                            }


                                        }else{

                                                $hotel_stat = "off-white";
                                                $error_count = 1;
                                                $ta_id_error .= $ta_id.', ';
                                                $date = date("YmdHis");

                                                $this->writeLog("Error Requested ".$ta_id_error." TA ID ",$date);

                                        }
                                    }
                                }
                                else
                                {
                                    $error_count = 1;
                                    $ta_id_error .= $ta_id.', ';
                                    $date = date("YmdHis");
                                    $this->writeLog("Error Requested ".$ta_id_error." TA ID ",$date);
                                }

                         }else{
                            $error_count = 1;
                            $ta_id_error .= $ta_id.', ';
                            $date = date("YmdHis");
                            $this->writeLog("Error Requested ".$ta_id_error." TA ID ",$date);
                         }
                }else{
                    $error_count = 1;
                    $ta_id_error .= $ta_id.', ';
                    $date = date("YmdHis");
                    $this->writeLog("Error Requested ".$ta_id_error." TA ID ",$date);
                }       
        }


            if($error_count > 0)
            {
                $ta_id_error_array = explode(',',str_replace(" ","",$ta_id_error));

                $ta_id_error_array = array_filter(array_unique($ta_id_error_array));

                $error_id_list = array_unique($ta_id_error_array);
                $error_id_list = implode(',',$error_id_list);

                $errors[] = array("error_code"=>3,
                    "message"=>"Hotel code ".$error_id_list." is no longer used.",
                    "timeout"=>600,
                    "hotel_ids"=>array_map('intval',$ta_id_error_array));
            }

            //echo array_count_values($hotels_array);

            $ta_id_array = array_map('intval',$ta_id_array);

            $num_hotels = count($hotels_array);

            // parsing to json
            $messages["api_version"] = 2;
            $messages["hotel_ids"] = array_unique($ta_id_array);
            $messages["start_date"] = date('Y-m-d',strtotime($start_date));
            $messages["end_date"] = date('Y-m-d',strtotime($end_date));
            $messages["lang"] = $lang;
            $messages["num_adults"] = intval($num_adults);
            $messages["num_hotels"] = $num_hotels;
            $messages["hotels"] = $hotels_array;
            $messages["errors"] = $errors;

         }

        // show response
        $this->response($messages, 200); // 200 being the HTTP response code
    }


 

    function writeLog($msg,$date)
    {
        $this->load->helper('file');
        if ( ! write_file('./errorslog/'.$date.'.txt', $msg))
        {
            return true;
        }
    }

    function get_ip_address() 
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe

                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }



      private function _checkHotelList($hotel_id)
    {
        //hotel list ID 12/11/2013 Array
        $hotels=array(
            "986427",
            "925002",
            "946868",
            "938430",
            "962611",
            "987252",
            "907588",
            "927832",
            "926120",
            "905322",
            "912933",
            "974315",
            "941222",
            "905885",
            "902677",
            "975432",
            "927616",
            "924800",
            "942073",
            "992855",
            "919040",
            "907108",
            "927960",
            "903868",
            "947884",
            "919298",
            "988404",
            "903388",
            "949000",
            "964301",
            "930037",
            "908073",
            "971310",
            "958994",
            "975837",
            "911740",
            "953280",
            "929864",
            "991361",
            "948556",
            "927020",
            "900518",
            "904519",
            "934678",
            "947330",
            "944746",
            "976988",
            "903390",
            "912609",
            "978982",
            "985992",
            "980572",
            "992062",
            "928730",
            "916946",
            "919777",
            "973215",
            "979399",
            "975130",
            "902136",
            "955276",
            "981476",
            "970800",
            "904077",
            "905674",
            "907300",
            "994545",
            "966288",
            "936901",
            "954858",
            "978634",
            "922912",
            "963648",
            "937650",
            "954120",
            "923214",
            "979842",
            "940173",
            "978128",
            "939422",
            "911761",
            "913120",
            "911175",
            "901355",
            "965402",
            "997564",
            "983760",
            "929128",
            "913297",
            "906582",
            "998884",
            "957792",
            "911780",
            "951296",
            "952620",
            "966570",
            "943712",
            "958816",
            "971621",
            "957742",
            "929672",
            "962464",
            "992100",
            "958708",
            "933392",
            "972195",
            "923394",
            "974640",
            "916563",
            "949496",
            "906056",
            "914268",
            "941248",
            "969640",
            "963364",
            "983146",
            "906790",
            "913708",
            "918656",
            "994968",
            "978086",
            "912380",
            "971162",
            "997894",
            "942946",
            "955573",
            "977420",
            "998068",
            "916178",
            "943346",
            "975837",
            "960752",
            "965936",
            "936072",
            "972984",
            "900745",
            "904490",
            "973728",
            "932728",
            "943800",
            "930379",
            "959782",
            "964085",
            "934576",
            "978740",
            "957651",
            "913213",
            "925258",
            "947851",
            "973596",
            "902030",
            "958720",
            "996886",
            "916881",
            "954241",
            "981405",
            "946868",
            "928770",
            "991004",
            "967340",
            "967320",
            "980950",
            "977392",
            "991612",
            "950345",
            "938536",
            "955672",
            "904717",
            "937415",
            "966090",
            "987718",
            "982096",
            "941007",
            "923740",
            "925180",
            "919486",
            "991003",
            "993932",
            "923613",
            "999782",
            "955665",
            "918515",
            "914052",
            "944150",
            "935820",
            "951050",
            "980418",
            "912680",
            "903582",
            "936752",
            "964048",
            "952872",
            "904120",
            "905072",
            "916258",
            "989127",
            "989126",
            "972565",
            "958917",
            "981161",
            "911595",
            "946946",
            "977256",
            "932150",
            "998558",
            "918048",
            "919480",
            "984392",
            "954742",
            "976005",
            "918720",
            "957154",
            "963346",
            "991376",
            "924954",
            "921112",
            "950268",
            "954455",
            "989708",
            "930264",
            "902145",
            "940230",
            "928976",
            "996468",
            "955792",
            "958398",
            "967370",
            "930379",
            "913240",
            "972614",
            "905697",
            "975167",
            "976374",
            "946372",
            "969918",
            "937130",
            "957960",
            "971592",
            "920976",
            "944574",
            "997120",
            "918878",
            "947816",
            "915051",
            "967508",
            "947637",
            "985092",
            "976936",
            "968175",
            "940230",
            "975624",
            "978027",
            "901072",
            "992911",
            "986248",
            "998292",
            "910110",
            "926584",
            "920740",
            "999726",
            "931390",
            "965806",
            "900520",
            "974848",
            "969660",
            "983627",
            "980328",
            "988668",
            "936838",
            "906962",
            "912962",
            "976115",
            "902947",
            "908416",
            "920469",
            "981672",
            "962881",
            "990396",
            "960346",
            "934800",
            "911110",
            "900108",
            "910060",
            "923365",
            "943486",
            "999676",
            "988092",
            "914044",
            "910687",
            "952092",
            "979320",
            "936662",
            "938936",
            "983627",
            "956499",
            "927831",
            "912592",
            "985973",
            "903423",
            "994676",
            "931721",
            "937415",
            "995510",
            "999205",
            "937589",
            "962144",
            "979429",
            "907588",
            "912706",
            "971608",
            "927352",
            "925361",
            "983035",
            "978884",
            "966244",
            "904320",
            "934845",
            "927066",
            "951172",
            "962611",
            "995530",
            "922672",
            "981556",
            "977591",
            "978755",
            "960744",
            "952120",
            "974992",
            "979298",
            "970925",
            "954458",
            "943060",
            "931932",
            "945112",
            "915608",
            "956716",
            "941386",
            "903883",
            "968307",
            "939853",
            "972900",
            "927670",
            "901584",
            "989199",
            "989846",
            "977022",
            "988523",
            "900415",
            "914070",
            "967728",
            "926608",
            "989365",
            "954536",
            "997365",
            "906032",
            "949584",
            "922544",
            "949765",
            "928304",
            "918472",
            "996207",
            "925280",
            "944835",
            "927370",
            "931116",
            "984456",
            "947788",
            "983384",
            "900830",
            "974196",
            "924307",
            "939853",
            "999806",
            "930920",
            "946148",
            "990188",
            "933720",
            "944588",
            "931720",
            "990910",
            "998530",
            "933070",
            "901292",
            "903681",
            "904070",
            "906264",
            "909236",
            "910105",
            "912577",
            "912644",
            "912855",
            "914314",
            "915546",
            "916414",
            "916984",
            "917645",
            "918771",
            "920816",
            "921491",
            "921508",
            "922772",
            "923261",
            "923783",
            "925024",
            "925232",
            "925648",
            "926324",
            "926910",
            "929288",
            "932600",
            "937715",
            "938725",
            "939352",
            "940396",
            "942638",
            "945140",
            "945800",
            "948144",
            "948477",
            "951394",
            "954132",
            "955913",
            "956452",
            "956907",
            "963716",
            "964240",
            "966646",
            "967944",
            "978537",
            "978922",
            "980732",
            "980800",
            "980855",
            "982020",
            "982212",
            "983265",
            "986348",
            "987726",
            "988412",
            "988987",
            "989068",
            "990084",
            "991293",
            "992784",
            "995686",
            "998710",
        );

        // if(in_array($hotel_id, $hotels))
        // {
        //     return true;
        // }else{
        //     return false;
        // }


        /* for this moment all true*/
        return true;

    }


function isSerializable($data)
{
    return (@unserialize($data) !== false);
}

function is_serialized( $data ) {
    // if it isn't a string, it isn't serialized
    if ( !is_string( $data ) )
        return false;
    $data = trim( $data );
    if ( 'N;' == $data )
        return true;
    if ( !preg_match( '/^([adObis]):/', $data, $badions ) )
        return false;
    switch ( $badions[1] ) {
        case 'a' :
        case 'O' :
        case 's' :
            if ( preg_match( "/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data ) )
                return true;
            break;
        case 'b' :
        case 'i' :
        case 'd' :
            if ( preg_match( "/^{$badions[1]}:[0-9.E-]+;\$/", $data ) )
                return true;
            break;
    }
    return false;
}


}