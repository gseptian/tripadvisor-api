<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package     CodeIgniter
 * @subpackage  Rest Server
 * @category    Controller
 * @author      Phil Sturgeon
 * @link        http://philsturgeon.co.uk/code/
 *
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Hotel_api_dev extends REST_Controller
{
    /* API response POST request */
    function checkHotel_post()
    {

        date_default_timezone_set('Asia/Jakarta');

        // POST Request from Trip Advisor
        //$ta_id = trim($this->post("ta_id")); // TripAdvisor hotel IDs (integer). Request by TripAdvisor
        $start_date = trim(date('d-M-Y',strtotime(isset($_POST["start_date"]) ? $_POST["start_date"] : " " ))); // Start date (yyyy-mm-dd). Request by TripAdvisor
        $end_date = trim(date('d-M-Y',strtotime(isset($_POST["end_date"]) ? $_POST["end_date"] : " " )));  // end date (yyyy-mm-dd). Request by TripAdvisor
        $year_stay = trim(date('Y',strtotime(isset($_POST["start_date"]) ? $_POST["start_date"] : " " )));
        $month_stay = trim(date('m',strtotime(isset($_POST["start_date"]) ? $_POST["start_date"] : " " )));
        $day_stay = trim(date('d',strtotime(isset($_POST["start_date"]) ? $_POST["start_date"] : " " )));
        $num_adults = trim(isset($_POST["num_adults"]) ? $_POST["num_adults"] : " " ); // number adults (integer). Request by TripAdvisor
        $key = base64_decode(trim(isset($_POST["query_key"]) ? $_POST["query_key"] : " " ));

        $lang = trim(isset($_POST["lang"]) ? $_POST["lang"] : "id_ID"  );
        // $lang = $lang == '' ? 'id_ID' : $lang;

        $d = $this->is_serialized(isset($_POST['hotels']) ? $_POST['hotels'] : " " );
        if($d==true)
        {
           $hotels_request = json_decode(unserialize(isset($_POST['hotels']) ? $_POST['hotels'] : " "));
        }else{
            $hotels_request = json_decode(isset($_POST['hotels']) ? $_POST['hotels'] : " ");
        }

        $dateNow = date('d-M-Y');

        $hotels_array=array();

        // count interval start date - end date
        $datetime1 = new DateTime($start_date);
        $datetime2 = new DateTime($end_date);
        $interval = $datetime1->diff($datetime2);
        $count = $interval->format('%a');

        $num_hotels = "";//1; // number hotel that had availabilty
        $hotels=$room_types="";
        $tax_val = $fee_val = 0;

        $num_hotels = count($hotels_request);

        if($num_hotels<=0)
        {
            $messages["errors"] = "Tripadvisor ID must be defined";
        }
        else
        {

            $no=0;
            $hotels_array=array();
            $errors = array();
            $error_count = 0;
            $ta_id_error = "";
            
            $start_date_db   = date('Ymd',strtotime($start_date));
            $end_date_db     = date('Ymd',strtotime($end_date));

            foreach($hotels_request as $value){

                $ta_id        = $value->ta_id;
                $partner_id   = $value->partner_id;
                $partner_url  = $value->partner_url;

                $ta_id_array[]=$ta_id; 

                // this is query
                 $hotels = $this->db->query("SELECT * FROM(SELECT RPDP.ADULT_SAL_PRICE_1 ADULT_UNIT_PRICE_1, 
                    RDS.SUPPLY_RM_CNT - RDS.RSV_RM_CNT STOCK_RMNDR_RM_CNT, 
                    PSC.SAL_LIMIT_SET_FLG ,  PSC.SAL_LIMIT_RMNDR_RM_CNT,
                    NVL(OYC.TAX_RATE,SAO.SERVICE_CHARGE) SERVICE_CHARGE, 
                    OYC.SETT_CRCY_CD, ER.TTS, YAM.KEN_CD, 
                    NVL(YKH.YAD_GRADE,'0') YAD_GRADE, 
                    nvl(SRP.OSUSUME_POINT2,0) OSUSUME_POINT, 
                    nvl(YCT.OSUSUME_POINT2,0) YAD_OSUSUME_POINT,
                    YKH.YAD_NAME,
                    JAT.TAID,
                    YCT.YAD_NO,
                    RPDP.STAY_DAY,
                    JRT.ROOM_TYPE_NAME,
                    SAO.sml_name,
                    LAO.lrg_name,
                    JTDK.seo_hotel_name,
                    JTDK.lrg_name as seo_lrg_name,
                    SRP.PLAN_CD,
                    SRP.ROOM_TYPE_CD,
                    CASE SRP.PRICE_NINMAI_FLG  WHEN '0' THEN '1'  ELSE '0'  END ROOM_CHARGE, NVL(SRP.WD_NET_PRICE_1, 0) WD_NET_PRICE_1
                     FROM J_YAD_CTL YCT 
                     INNER JOIN J_OUT_YAD_CTL OYC ON OYC.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_YAD_KHN YKH ON YKH.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_SEARCH_ROOM_PLAN SRP ON SRP.YAD_NO = YKH.YAD_NO 
                     INNER JOIN J_ROOM_PLAN_DAY_PRICE RPDP ON SRP.YAD_NO = RPDP.YAD_NO AND SRP.PLAN_CD = RPDP.PLAN_CD AND SRP.ROOM_TYPE_CD = RPDP.ROOM_TYPE_CD 
                     INNER JOIN J_ROOM_DAY_STOCK RDS ON RPDP.YAD_NO = RDS.YAD_NO AND RPDP.ROOM_TYPE_CD = RDS.ROOM_TYPE_CD AND RPDP.STAY_DAY = RDS.STAY_DAY 
                     INNER JOIN J_PLAN_SAL_CONTROL PSC ON RPDP.YAD_NO = PSC.YAD_NO AND RPDP.PLAN_CD = PSC.PLAN_CD AND RPDP.STAY_DAY = PSC.STAY_DAY 
                     INNER JOIN J_YAD_AREA_MAP YAM ON YAM.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_SML_AREA_OUT SAO ON SAO.SML_CD = YAM.SML_CD 
                     INNER JOIN j_LRG_AREA_OUT_trans LAO ON LAO.LRG_CD = YAM.LRG_CD 
                     INNER JOIN J_TIME_DIFF TD ON TD.LRG_CD = YAM.LRG_CD 
                     INNER JOIN J_EX_RATE ER ON ER.CRCY_CD = OYC.SETT_CRCY_CD
                     INNER JOIN USR_JIDSRV01.j_tripadvisor_hotel JAT ON JAT.YAD_NO = YCT.YAD_NO
                     INNER JOIN J_ROOM_TYPE JRT ON JRT.ROOM_TYPE_CD = RPDP.ROOM_TYPE_CD
                     LEFT OUTER JOIN J_TDK JTDK ON JTDK.YAD_NO = YCT.YAD_NO
                     WHERE YCT.REF_FLG = '1' 
                        AND (YKH.YAD_SYU_CD IS NULL OR YKH.YAD_SYU_CD <> '13') 
                        AND NOT(NVL(RDS.ARVL_STOP_FLG,0) = 1 
                        AND RDS.STAY_DAY = TO_DATE('$start_date_db','YYYYMMDD')) 
                        AND YCT.COUNTRY_FLG = '1' 
                        AND SRP.ROOM_CAPA_MIN_NUM <= to_number('$num_adults') 
                        AND SRP.ROOM_CAPA_MOST_NUM >= to_number('$num_adults') 
                        AND SRP.RSV_RCT_ENBL_FLG = '1' 
                        --AND RPDP.STAY_DAY BETWEEN TO_DATE('$start_date_db','YYYYMMDD') 
                        --AND TO_DATE('$end_date_db','YYYYMMDD') 
                        AND RPDP.STAY_DAY < TO_DATE('$end_date_db' ,'YYYYMMDD') 
                        AND RPDP.STAY_DAY >= TO_DATE('$start_date_db' ,'YYYYMMDD')  
                        AND SRP.SPLY_PERIOD_STR_DAY < TO_DATE('$end_date_db' ,'YYYYMMDD') 
                        AND SRP.SPLY_PERIOD_END_DAY >= TO_DATE('$start_date_db' ,'YYYYMMDD')  
                        AND( TO_DATE('$end_date_db' ,'YYYYMMDD') - SRP.RSV_END_DAY + round(SRP.RSV_END_HH / 100,0)/24 + mod(SRP.RSV_END_HH,100)/1440  >= TO_DATE('$start_date_db','YYYYMMDD') + TO_NUMBER('09')/24 + (TD.TIME_DIFF - 8)/24 + TO_NUMBER('43')/1440 ) 
                        AND RPDP.SAL_STAT_CD = '0' AND RDS.SAL_STAT_CD = '0' AND PSC.SAL_STAT_CD = '0' AND RDS.SUPPLY_RM_CNT - RDS.RSV_RM_CNT >= TO_NUMBER('$count') 
                        AND ( RDS.LNG_STY_LIMIT_FLG <> '1'  OR RDS.SHORT_LNG_STY <= TO_NUMBER('$count') ) 
                        AND SRP.SECRET_PLAN_FLG = '0' 
                        AND JAT.YAD_NO ='".$partner_id."'
                        AND JAT.TAID ='".$ta_id."'
                        AND RPDP.ADULT_SAL_PRICE_1 is not NULL 
                        ORDER BY ADULT_UNIT_PRICE_1 ASC, RPDP.STAY_DAY ASC) WHERE ROWNUM <=1 ");

//echo $this->db->last_query();  AND JAT.YAD_NO='".$partner_id."'
// echo "<pre>";
// print_r($hotels);
// echo "</pre>";
// die();
                
                /* for check available */
                $query_stay = $this->db->query("SELECT 
                    RPDP.STAY_DAY
                     FROM J_YAD_CTL YCT 
                     INNER JOIN J_OUT_YAD_CTL OYC ON OYC.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_YAD_KHN YKH ON YKH.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_SEARCH_ROOM_PLAN SRP ON SRP.YAD_NO = YKH.YAD_NO 
                     INNER JOIN J_ROOM_PLAN_DAY_PRICE RPDP ON SRP.YAD_NO = RPDP.YAD_NO AND SRP.PLAN_CD = RPDP.PLAN_CD AND SRP.ROOM_TYPE_CD = RPDP.ROOM_TYPE_CD 
                     INNER JOIN J_ROOM_DAY_STOCK RDS ON RPDP.YAD_NO = RDS.YAD_NO AND RPDP.ROOM_TYPE_CD = RDS.ROOM_TYPE_CD AND RPDP.STAY_DAY = RDS.STAY_DAY 
                     INNER JOIN J_PLAN_SAL_CONTROL PSC ON RPDP.YAD_NO = PSC.YAD_NO AND RPDP.PLAN_CD = PSC.PLAN_CD AND RPDP.STAY_DAY = PSC.STAY_DAY 
                     INNER JOIN J_YAD_AREA_MAP YAM ON YAM.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_SML_AREA_OUT SAO ON SAO.SML_CD = YAM.SML_CD 
                     INNER JOIN j_LRG_AREA_OUT_trans LAO ON LAO.LRG_CD = YAM.LRG_CD 
                     INNER JOIN J_TIME_DIFF TD ON TD.LRG_CD = YAM.LRG_CD 
                     INNER JOIN J_EX_RATE ER ON ER.CRCY_CD = OYC.SETT_CRCY_CD
                     INNER JOIN USR_JIDSRV01.j_tripadvisor_hotel JAT ON JAT.YAD_NO = YCT.YAD_NO
                     INNER JOIN J_ROOM_TYPE JRT ON JRT.ROOM_TYPE_CD = RPDP.ROOM_TYPE_CD
                     LEFT OUTER JOIN J_TDK JTDK ON JTDK.YAD_NO = YCT.YAD_NO
                     WHERE YCT.REF_FLG = '1' 
                        AND (YKH.YAD_SYU_CD IS NULL OR YKH.YAD_SYU_CD <> '13') 
                        AND NOT(NVL(RDS.ARVL_STOP_FLG,0) = 1 
                        AND RDS.STAY_DAY = TO_DATE('$start_date_db','YYYYMMDD')) 
                        AND YCT.COUNTRY_FLG = '1' 
                        AND SRP.ROOM_CAPA_MIN_NUM <= to_number('$num_adults') 
                        AND SRP.ROOM_CAPA_MOST_NUM >= to_number('$num_adults') 
                        AND SRP.RSV_RCT_ENBL_FLG = '1' 
                        --AND RPDP.STAY_DAY BETWEEN TO_DATE('$start_date_db','YYYYMMDD') 
                        --AND TO_DATE('$end_date_db','YYYYMMDD') 
                        AND RPDP.STAY_DAY < TO_DATE('$end_date_db' ,'YYYYMMDD') 
                        AND RPDP.STAY_DAY >= TO_DATE('$start_date_db' ,'YYYYMMDD')  
                        AND SRP.SPLY_PERIOD_STR_DAY < TO_DATE('$end_date_db' ,'YYYYMMDD') 
                        AND SRP.SPLY_PERIOD_END_DAY >= TO_DATE('$start_date_db' ,'YYYYMMDD')  
                        AND( TO_DATE('$end_date_db' ,'YYYYMMDD') - SRP.RSV_END_DAY + round(SRP.RSV_END_HH / 100,0)/24 + mod(SRP.RSV_END_HH,100)/1440  >= TO_DATE('$start_date_db','YYYYMMDD') + TO_NUMBER('09')/24 + (TD.TIME_DIFF - 8)/24 + TO_NUMBER('43')/1440 ) 
                        AND RPDP.SAL_STAT_CD = '0' AND RDS.SAL_STAT_CD = '0' AND PSC.SAL_STAT_CD = '0' AND RDS.SUPPLY_RM_CNT - RDS.RSV_RM_CNT >= TO_NUMBER('$count') 
                        AND ( RDS.LNG_STY_LIMIT_FLG <> '1'  OR RDS.SHORT_LNG_STY <= TO_NUMBER('$count') ) 
                        AND SRP.SECRET_PLAN_FLG = '0' 
                        AND JAT.TAID ='".$ta_id."'
                        AND JAT.YAD_NO ='".$partner_id."'
                        AND RPDP.ADULT_SAL_PRICE_1 is not NULL 
                        GROUP BY RPDP.STAY_DAY
                        ");
//  ND OYC.SETT_CRCY_CD ='USD' WHERE ROWNUM <=1
// usd ta id  2350943



                if(($hotels->num_rows() > 0) && ($query_stay->num_rows() >= $count) && ( $count <= 9) ) 
                {
                    $t = "";

                    $hotel_stat = "on";
                    $num_hotels_count=0;
                    foreach ($hotels->result() as $hotel) {

                        $lrg_name =  str_replace("-","_",url_title(strtolower($hotel->LRG_NAME)));

                        // hotel name key
                        if($hotel->SEO_HOTEL_NAME)
                        {
                            $hotel_name = str_replace("-","_",url_title(strtolower($hotel->SEO_HOTEL_NAME)));
                            $hotel_id = $hotel->YAD_NO;
                            $hotelNameKey = rawurlencode($hotel->SEO_HOTEL_NAME);

                            if($hotel->SEO_LRG_NAME)
                            {
                                $seo_lrg_name =  str_replace("-","_",url_title(strtolower($hotel->SEO_LRG_NAME)));
                            }else{
                                $seo_lrg_name =  str_replace("-","_",url_title(strtolower($hotel->LRG_NAME)));
                            }

                            $seo_hotel_name   = $hotel_name.'_'.$seo_lrg_name;
                            
                        }else{
                            $hotel_name = str_replace("-","_",url_title(strtolower($hotel->YAD_NAME)));
                            $hotel_id = $hotel->YAD_NO;
                            $hotelNameKey = rawurlencode($hotel->YAD_NAME);

                            $seo_hotel_name   = $hotel_name;
                        }

                        // url detail to pegipegi.com
                        $url = "http://www.pegipegi.com/hotel/".$lrg_name."/".$seo_hotel_name."_".$hotel_id."/?stayYear=".$year_stay."&stayMonth=".$month_stay."&stayDay=".$day_stay."&stayCount=". $count."&roomCrack=".$num_adults."00000&utm_source=alliance&utm_medium=tripadvisor&utm_campaign=".$hotel_id."";

                        $fee=$tax=$final_price="";

                        //check the hotel id, if doesn't set in hotel list, return to error
                        if($this->_checkHotelList($hotel_id)==true)
                        {
                            $hotel_stat = "on-white";
                            $no++;
                            // get tax and fee
                            //                    $taxFee = $this->_getTaxAndFee($hotel_id);
                            //                    foreach($taxFee as $tf)
                            //                    {
                            //                        $tax_val = $tf->TAX;
                            //                        $fee_val = $tf->serviceCharge;
                            //                    }

                            $room_type_cd = $hotel->ROOM_TYPE_CD;
                            $room_plan_cd = $hotel->PLAN_CD;

                            $get_price = $this->getPrice($hotel_id,$room_type_cd,$room_plan_cd,$num_adults,$start_date_db,$end_date_db,$count);

                            $avg_price = $get_price['avg_price'];
                            $sum_price = $get_price['sum_price'];
                            
                           // echo $avg_price."<br>";

                            # tax
                            if($hotel->SETT_CRCY_CD=='USD')
                            {
                                $price_days = $sum_price;
                                $price_net  = $avg_price * $hotel->TTS;

                                //$price_idr = $this->bigDecimal($price_days) * $this->bigDecimal($hotel->TTS);
                                // $price_idr = $price_days * $hotel->TTS;
                                // echo $totalval  = $price_idr * 21;
                                
                                //echo number_format($totalval, 1, '.', '');
                                //die();
                                # tax
                                $tax_usd = ($price_days * 10) / 100;
                                $tax     = floor(($tax_usd * $hotel->TTS));
                                $tax_net = ($price_net * 10) / 100;

                                # fee
                                $fee_usd = ($price_days * $hotel->SERVICE_CHARGE )/100;
                                $fee     = floor(($fee_usd * $hotel->TTS));
                                $fee_big = $this->bigDecimal($fee_usd) * $this->bigDecimal($hotel->TTS);
                                $fee_net = ($price_net  * $hotel->SERVICE_CHARGE )/100;

                                /* convert to IDR price USD * TTS  */
                                $real_price = $price_days * $hotel->TTS;

                                # final price
                                $final_price = $real_price + $tax + $fee;

                            }else{

                                $price_days = $sum_price;
                                $price_net = $avg_price;

                                # tax
                                $tax = ( $price_days * 10 )/100;
                                $tax = $tax;

                                $tax_net = round( ($price_net * 10) /100);

                                # fee
                                $fee = ( $price_days * $hotel->SERVICE_CHARGE )/100;
                                $fee = $fee;

                                $fee_net = round( ($price_net  * $hotel->SERVICE_CHARGE )/100);

                                # final price
                                $totalval       = $hotel->SERVICE_CHARGE + 10;
                                $totalval       = ($price_days * $totalval) / 100; 
                                $final_price    = $price_days + round($totalval);
                                //$final_price = $price_days + $tax + $fee;
                            }    

                           

                            // room name list
                            $hotel->ROOM_TYPE_NAME;
                            // $room_types[$hotel->ROOM_TYPE_NAME] = $detail;

                            $room_name = $hotel->ROOM_TYPE_NAME;

                        }else{

                            $hotel_stat = "off-white";
                            $error_count = 1;
                            $ta_id_error .= $ta_id.', ';
                            $date = date("YmdHis");

                            $this->writeLog("Error Requested ".$ta_id_error." TA ID ",$date);

                        }

                    }

                    if($this->_checkHotelList($hotel_id)==true)
                    {
                        $detail = array(
                            "url" =>$url, // url hotel id
                            "price"=>intval($price_net), // price
                            "fees"=>intval($fee_net), // fees
                            "taxes"=>intval($tax_net), // taxes
                            "final_price"=>intval($final_price), // final price
                            "currency"=>"IDR" // currency format/code
                        );

                        $room_types = array( $hotel->ROOM_TYPE_NAME => $detail);
                        $hotels_array[] = array("hotel_id"=>intval($ta_id),'room_types'=>$room_types);


                    }


                }
                else
                {
                    $error_count = 1;
                    $ta_id_error .= $ta_id.', ';
                    $date = date("YmdHis");
                    $this->writeLog("Error Requested ".$ta_id_error." TA ID ",$date);
                }


        }


            if($error_count > 0)
            {
                $ta_id_error_array = explode(',',str_replace(" ","",$ta_id_error));

                $ta_id_error_array = array_filter(array_unique($ta_id_error_array));

                $error_id_list = array_unique($ta_id_error_array);
                $error_id_list = implode(',',$error_id_list);

                $errors[] = array("error_code"=>3,
                    "message"=>"Hotel code ".$error_id_list." is no longer used.",
                    "timeout"=>600,
                    "hotel_ids"=>array_map('intval',$ta_id_error_array));
            }

            //echo array_count_values($hotels_array);

            $ta_id_array = array_map('intval',$ta_id_array);

            $num_hotels = count($hotels_array);

            // parsing to json
            $messages["api_version"] = 2;
            $messages["hotel_ids"] = array_unique($ta_id_array);
            $messages["start_date"] = date('Y-m-d',strtotime($start_date));
            $messages["end_date"] = date('Y-m-d',strtotime($end_date));
            $messages["lang"] = $lang;
            $messages["num_adults"] = intval($num_adults);
            $messages["num_hotels"] = $num_hotels;
            $messages["hotels"] = $hotels_array;
            $messages["errors"] = $errors;

         }

        // show response
        $this->response($messages, 200); // 200 being the HTTP response code
    }

    function bigDecimal($val)
    {

        return number_format($val, 5, '', '');
    
    }

    function getPrice($hotel_id,$room_type_cd,$room_plan_cd,$num_adults,$start_date_db,$end_date_db,$count)
    {



       // echo $room_type_cd." | ".$room_plan_cd;
                    $query = $this->db->query("SELECT RPDP.ADULT_SAL_PRICE_1 ADULT_UNIT_PRICE_1
                     FROM J_YAD_CTL YCT 
                     INNER JOIN J_OUT_YAD_CTL OYC ON OYC.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_YAD_KHN YKH ON YKH.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_SEARCH_ROOM_PLAN SRP ON SRP.YAD_NO = YKH.YAD_NO 
                     INNER JOIN J_ROOM_PLAN_DAY_PRICE RPDP ON SRP.YAD_NO = RPDP.YAD_NO AND SRP.PLAN_CD = RPDP.PLAN_CD AND SRP.ROOM_TYPE_CD = RPDP.ROOM_TYPE_CD 
                     INNER JOIN J_ROOM_DAY_STOCK RDS ON RPDP.YAD_NO = RDS.YAD_NO AND RPDP.ROOM_TYPE_CD = RDS.ROOM_TYPE_CD AND RPDP.STAY_DAY = RDS.STAY_DAY 
                     INNER JOIN J_PLAN_SAL_CONTROL PSC ON RPDP.YAD_NO = PSC.YAD_NO AND RPDP.PLAN_CD = PSC.PLAN_CD AND RPDP.STAY_DAY = PSC.STAY_DAY 
                     INNER JOIN J_YAD_AREA_MAP YAM ON YAM.YAD_NO = YCT.YAD_NO 
                     INNER JOIN J_SML_AREA_OUT SAO ON SAO.SML_CD = YAM.SML_CD 
                     INNER JOIN j_LRG_AREA_OUT_trans LAO ON LAO.LRG_CD = YAM.LRG_CD 
                     INNER JOIN J_TIME_DIFF TD ON TD.LRG_CD = YAM.LRG_CD 
                     INNER JOIN J_EX_RATE ER ON ER.CRCY_CD = OYC.SETT_CRCY_CD
                     INNER JOIN USR_JIDSRV01.j_tripadvisor_hotel JAT ON JAT.YAD_NO = YCT.YAD_NO
                     INNER JOIN J_ROOM_TYPE JRT ON JRT.ROOM_TYPE_CD = RPDP.ROOM_TYPE_CD
                     LEFT OUTER JOIN J_TDK JTDK ON JTDK.YAD_NO = YCT.YAD_NO
                     WHERE YCT.REF_FLG = '1' 
                        AND (YKH.YAD_SYU_CD IS NULL OR YKH.YAD_SYU_CD <> '13') 
                        AND NOT(NVL(RDS.ARVL_STOP_FLG,0) = 1 
                        AND RDS.STAY_DAY = TO_DATE('$start_date_db','YYYYMMDD')) 
                        AND YCT.COUNTRY_FLG = '1' 
                        AND SRP.ROOM_CAPA_MIN_NUM <= to_number('$num_adults') 
                        AND SRP.ROOM_CAPA_MOST_NUM >= to_number('$num_adults') 
                        AND SRP.RSV_RCT_ENBL_FLG = '1' 
                        --AND RPDP.STAY_DAY BETWEEN TO_DATE('$start_date_db','YYYYMMDD') 
                        --AND TO_DATE('$end_date_db','YYYYMMDD') 
                        AND RPDP.STAY_DAY < TO_DATE('$end_date_db' ,'YYYYMMDD') 
                        AND RPDP.STAY_DAY >= TO_DATE('$start_date_db' ,'YYYYMMDD')  
                        AND SRP.SPLY_PERIOD_STR_DAY < TO_DATE('$end_date_db' ,'YYYYMMDD') 
                        AND SRP.SPLY_PERIOD_END_DAY >= TO_DATE('$start_date_db' ,'YYYYMMDD')  
                        AND( TO_DATE('$end_date_db' ,'YYYYMMDD') - SRP.RSV_END_DAY + round(SRP.RSV_END_HH / 100,0)/24 + mod(SRP.RSV_END_HH,100)/1440  >= TO_DATE('$start_date_db','YYYYMMDD') + TO_NUMBER('09')/24 + (TD.TIME_DIFF - 8)/24 + TO_NUMBER('43')/1440 ) 
                        AND RPDP.SAL_STAT_CD = '0' AND RDS.SAL_STAT_CD = '0' AND PSC.SAL_STAT_CD = '0' AND RDS.SUPPLY_RM_CNT - RDS.RSV_RM_CNT >= TO_NUMBER('$count') 
                        AND ( RDS.LNG_STY_LIMIT_FLG <> '1'  OR RDS.SHORT_LNG_STY <= TO_NUMBER('$count') ) 
                        AND SRP.SECRET_PLAN_FLG = '0' 
                        AND YCT.YAD_NO ='".$hotel_id."'
                        AND SRP.ROOM_TYPE_CD = '".$room_type_cd."'
                        AND SRP.PLAN_CD = '".$room_plan_cd."'
                        AND RPDP.ADULT_SAL_PRICE_1 is not NULL 
                        ORDER BY ADULT_UNIT_PRICE_1 ASC, RPDP.STAY_DAY ASC");


  
                    $count = $query->num_rows();

                    $price = 0;
                    foreach ($query->result() as $value) {
                        # code...
                        $price = $value->ADULT_UNIT_PRICE_1 + $price;
                         //echo $value->STAY_DAY."  ".$value->ADULT_UNIT_PRICE_1."<br>";

                    }

                    if($price > 0 || $count > 0)
                    {
                        $avg['avg_price'] = $price / $count;
                    }else{
                        $avg['avg_price'] = 0;
                    }
                    
                    $avg['count']     = $count;
                    $avg['sum_price'] = $price;
                    return $avg;
        
    }

    function writeLog($msg,$date)
    {
        $this->load->helper('file');
        if ( ! write_file('./errorslog/'.$date.'.txt', $msg))
        {
            return true;
        }
    }


    /* Just for example API response. GET request */
    function checkHotelGet_get()
    {

        $ta_id = $this->get("ta_id");
        $start_date = $this->get("start_date");
        $end_date = $this->get("end_date");
        $num_adults = $this->get("num_adults");
        $num_hotels = 1;
        $hotels=array();

        //check available hotel in database
        $this->db->where('ta_id',$ta_id);
        $query = $this->db->get("hotellist");
        if($query->num_rows() > 0)
        {

            $url = base_url()."testform/detail/".$ta_id;
            $detail = array(
                        "url" =>$url,
                        "price"=>"1000",
                        "fees"=>"101",
                        "taxes"=>"5",
                        "final_price"=>"1106",
                        "currency"=>"IDR"

            );
            $room_types = array("Fenway Room"=>$detail);
            $hotels = array("hotel_id"=>$ta_id,"room_types"=>$room_types);

            $errors = "";
        }
        else
        {
            $errors = array("error_code"=>3,
                            "message"=>"Hotel code ".$ta_id." is no longer used.",
                            "timeout"=>600,
                            "hotel_ids"=>$ta_id);
        }

        $num_hotels2 = count($hotels);

        $messages["api_version"] = 2;
        $messages["hotel_ids"] = array($ta_id);
        $messages["start_date"] = $start_date;
        $messages["end_date"] = $end_date;
        $messages["lang"] = $lang;
        $messages["num_adults"] = $num_adults;
        $messages["num_hotels"] = $num_hotels2;
        $messages["hotels"] = $hotels;
        $messages["errors"] = array_unique($errors);
        $this->response($messages, 200); // 200 being the HTTP response code
    }

    private function _getTaxAndFee($yad_no)
    {
        $taxAndfee = $this->db->query("select j_value_add_tax_mst.value_add_tax as tax,
        NVL(j_out_yad_ctl.tax_rate,
        j_sml_area_out.service_charge) as serviceCharge,
        j_out_yad_ctl.sys_use_rate as comission
        from j_yad_area_map, j_value_add_tax_mst, j_out_yad_ctl, j_sml_area_out
        where j_out_yad_ctl.yad_no = ".$yad_no."
        ");

        return $taxAndfee->result();
    }

    private function pembulatan($price)
    {
        $ratusan = substr($price, -3);
        if($ratusan<500)$akhir = $price - $ratusan;
        else $akhir = $price + (1000-$ratusan);
        //return number_format($akhir, 0, ',', ',');
        return $akhir;
    }

    function get_ip_address() 
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe

                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }



      private function _checkHotelList($hotel_id)
    {
        //hotel list ID 12/11/2013 Array
        $hotels=array(
            "986427",
            "925002",
            "946868",
            "938430",
            "962611",
            "987252",
            "907588",
            "927832",
            "926120",
            "905322",
            "912933",
            "974315",
            "941222",
            "905885",
            "902677",
            "975432",
            "927616",
            "924800",
            "942073",
            "992855",
            "919040",
            "907108",
            "927960",
            "903868",
            "947884",
            "919298",
            "988404",
            "903388",
            "949000",
            "964301",
            "930037",
            "908073",
            "971310",
            "958994",
            "975837",
            "911740",
            "953280",
            "929864",
            "991361",
            "948556",
            "927020",
            "900518",
            "904519",
            "934678",
            "947330",
            "944746",
            "976988",
            "903390",
            "912609",
            "978982",
            "985992",
            "980572",
            "992062",
            "928730",
            "916946",
            "919777",
            "973215",
            "979399",
            "975130",
            "902136",
            "955276",
            "981476",
            "970800",
            "904077",
            "905674",
            "907300",
            "994545",
            "966288",
            "936901",
            "954858",
            "978634",
            "922912",
            "963648",
            "937650",
            "954120",
            "923214",
            "979842",
            "940173",
            "978128",
            "939422",
            "911761",
            "913120",
            "911175",
            "901355",
            "965402",
            "997564",
            "983760",
            "929128",
            "913297",
            "906582",
            "998884",
            "957792",
            "911780",
            "951296",
            "952620",
            "966570",
            "943712",
            "958816",
            "971621",
            "957742",
            "929672",
            "962464",
            "992100",
            "958708",
            "933392",
            "972195",
            "923394",
            "974640",
            "916563",
            "949496",
            "906056",
            "914268",
            "941248",
            "969640",
            "963364",
            "983146",
            "906790",
            "913708",
            "918656",
            "994968",
            "978086",
            "912380",
            "971162",
            "997894",
            "942946",
            "955573",
            "977420",
            "998068",
            "916178",
            "943346",
            "975837",
            "960752",
            "965936",
            "936072",
            "972984",
            "900745",
            "904490",
            "973728",
            "932728",
            "943800",
            "930379",
            "959782",
            "964085",
            "934576",
            "978740",
            "957651",
            "913213",
            "925258",
            "947851",
            "973596",
            "902030",
            "958720",
            "996886",
            "916881",
            "954241",
            "981405",
            "946868",
            "928770",
            "991004",
            "967340",
            "967320",
            "980950",
            "977392",
            "991612",
            "950345",
            "938536",
            "955672",
            "904717",
            "937415",
            "966090",
            "987718",
            "982096",
            "941007",
            "923740",
            "925180",
            "919486",
            "991003",
            "993932",
            "923613",
            "999782",
            "955665",
            "918515",
            "914052",
            "944150",
            "935820",
            "951050",
            "980418",
            "912680",
            "903582",
            "936752",
            "964048",
            "952872",
            "904120",
            "905072",
            "916258",
            "989127",
            "989126",
            "972565",
            "958917",
            "981161",
            "911595",
            "946946",
            "977256",
            "932150",
            "998558",
            "918048",
            "919480",
            "984392",
            "954742",
            "976005",
            "918720",
            "957154",
            "963346",
            "991376",
            "924954",
            "921112",
            "950268",
            "954455",
            "989708",
            "930264",
            "902145",
            "940230",
            "928976",
            "996468",
            "955792",
            "958398",
            "967370",
            "930379",
            "913240",
            "972614",
            "905697",
            "975167",
            "976374",
            "946372",
            "969918",
            "937130",
            "957960",
            "971592",
            "920976",
            "944574",
            "997120",
            "918878",
            "947816",
            "915051",
            "967508",
            "947637",
            "985092",
            "976936",
            "968175",
            "940230",
            "975624",
            "978027",
            "901072",
            "992911",
            "986248",
            "998292",
            "910110",
            "926584",
            "920740",
            "999726",
            "931390",
            "965806",
            "900520",
            "974848",
            "969660",
            "983627",
            "980328",
            "988668",
            "936838",
            "906962",
            "912962",
            "976115",
            "902947",
            "908416",
            "920469",
            "981672",
            "962881",
            "990396",
            "960346",
            "934800",
            "911110",
            "900108",
            "910060",
            "923365",
            "943486",
            "999676",
            "988092",
            "914044",
            "910687",
            "952092",
            "979320",
            "936662",
            "938936",
            "983627",
            "956499",
            "927831",
            "912592",
            "985973",
            "903423",
            "994676",
            "931721",
            "937415",
            "995510",
            "999205",
            "937589",
            "962144",
            "979429",
            "907588",
            "912706",
            "971608",
            "927352",
            "925361",
            "983035",
            "978884",
            "966244",
            "904320",
            "934845",
            "927066",
            "951172",
            "962611",
            "995530",
            "922672",
            "981556",
            "977591",
            "978755",
            "960744",
            "952120",
            "974992",
            "979298",
            "970925",
            "954458",
            "943060",
            "931932",
            "945112",
            "915608",
            "956716",
            "941386",
            "903883",
            "968307",
            "939853",
            "972900",
            "927670",
            "901584",
            "989199",
            "989846",
            "977022",
            "988523",
            "900415",
            "914070",
            "967728",
            "926608",
            "989365",
            "954536",
            "997365",
            "906032",
            "949584",
            "922544",
            "949765",
            "928304",
            "918472",
            "996207",
            "925280",
            "944835",
            "927370",
            "931116",
            "984456",
            "947788",
            "983384",
            "900830",
            "974196",
            "924307",
            "939853",
            "999806",
            "930920",
            "946148",
            "990188",
            "933720",
            "944588",
            "931720",
            "990910",
            "998530",
            "933070",
            "901292",
            "903681",
            "904070",
            "906264",
            "909236",
            "910105",
            "912577",
            "912644",
            "912855",
            "914314",
            "915546",
            "916414",
            "916984",
            "917645",
            "918771",
            "920816",
            "921491",
            "921508",
            "922772",
            "923261",
            "923783",
            "925024",
            "925232",
            "925648",
            "926324",
            "926910",
            "929288",
            "932600",
            "937715",
            "938725",
            "939352",
            "940396",
            "942638",
            "945140",
            "945800",
            "948144",
            "948477",
            "951394",
            "954132",
            "955913",
            "956452",
            "956907",
            "963716",
            "964240",
            "966646",
            "967944",
            "978537",
            "978922",
            "980732",
            "980800",
            "980855",
            "982020",
            "982212",
            "983265",
            "986348",
            "987726",
            "988412",
            "988987",
            "989068",
            "990084",
            "991293",
            "992784",
            "995686",
            "998710",
        );

        // if(in_array($hotel_id, $hotels))
        // {
        //     return true;
        // }else{
        //     return false;
        // }


        /* for this moment all true*/
        return true;

    }


function isSerializable($data)
{
    return (@unserialize($data) !== false);
}

function is_serialized( $data ) {
    // if it isn't a string, it isn't serialized
    if ( !is_string( $data ) )
        return false;
    $data = trim( $data );
    if ( 'N;' == $data )
        return true;
    if ( !preg_match( '/^([adObis]):/', $data, $badions ) )
        return false;
    switch ( $badions[1] ) {
        case 'a' :
        case 'O' :
        case 's' :
            if ( preg_match( "/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data ) )
                return true;
            break;
        case 'b' :
        case 'i' :
        case 'd' :
            if ( preg_match( "/^{$badions[1]}:[0-9.E-]+;\$/", $data ) )
                return true;
            break;
    }
    return false;
}


}