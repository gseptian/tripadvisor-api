<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class testform extends CI_Controller {

	 public function __construct(){
        parent::__construct();
    }
	

	function index_off()
	{

		echo "
		<h1>Request Tripadvisor API POST</h1>
		<form action='".base_url()."hotel_availability_testing' method='POST'>
		Trip Advisor Hotel ID Array (input ID separated by comma. This is will generate to array. Only for test) <br><input type='text' name='ta_id'> Request by Tripadvisor. <br>
		<small><i>ex:Tripadvisor ID = 634509,586362 ( format : array integer )</i></small><br><br>
		Start Date<br><input type='text' name='start_date'> Request by Tripadvisor<br>
		<small><i>ex:start date = 2014-01-15  ( format : yyyy-mm-dd )</i></small><br>
		<br>End Date<br><input type='text' name='end_date'> Request by Tripadvisor<br>
		<small><i>ex:end date = 2014-01-17  ( format : yyyy-mm-dd )</i></small><br><br>
		Num Adults<br><input type='text' name='num_adults'> Request by Tripadvisor<br>
		<small><i>ex:num = 1  ( format : integer )</i></small><br><br>
		Key<br><input type='text' name='query_key'> Request by Tripadvisor<br>
		<small><i>ex: cGVnaXBlZ2lrZXlhcGk=  (format : string ) </i></small><br><br>
		<input type='submit' value='send'>
		</form>
		";
	}

    function index()
    {
    	//$this->samplecurl();
        echo "
		<h1>Request Tripadvisor API POST</h1>
		<form action='".base_url()."hotel_availability' method='POST'>
		Trip Advisor Hotel ID Array<br>
		<input type='text' name='ta_id' value='634509'><br>
		<input type='text' name='ta_id' value='586362'><br>
		<input type='text' name='ta_id' value=''><br> Request by Tripadvisor. <br>
		<small><i>format : array integer</i></small><br><br>
		Start Date<br><input type='text' name='start_date' value='2014-01-15'> Request by Tripadvisor<br>
		<small><i>ex:start date = 2014-01-15  ( format : yyyy-mm-dd )</i></small><br>
		<br>End Date<br><input type='text' name='end_date' value='2014-01-17'> Request by Tripadvisor<br>
		<small><i>ex:end date = 2014-01-17  ( format : yyyy-mm-dd )</i></small><br><br>
		Num Adults<br><input type='text' name='num_adults' value='1'> Request by Tripadvisor<br>
		<small><i>ex:num = 1  ( format : integer )</i></small><br><br>
		Key<br><input type='text' name='query_key' value='cGVnaXBlZ2lrZXlhcGk='> Request by Tripadvisor<br>
		<small><i>ex: cGVnaXBlZ2lrZXlhcGk=  (format : string ) </i></small><br><br>
		<input type='submit' value='send'>
		</form>
		";
    }

    function samplecurl()
    {

        echo "
		<h1>Request Tripadvisor API POST (Curl)</h1>
		<br>
		<style>
			tr, td{
				padding: 5px;
			}
		</style>
		<table border=1 style='border-collapse: collapse;'>
			<tr>
				<td><b>API url</b></td><td>:</td><td><a href='http://www.pegipegi.com/tripadvisor/hotel_availability/'>http://www.pegipegi.com/tripadvisor/hotel_availability/</a></td>
			</tr>
			<tr>
				<td><b>Methode</b></td><td>:</td><td>POST</td>
			</tr>
			<tr>
				<td><b>Response</b></td><td>:</td><td>JSON</td>
			</tr>

		</table>
		<br><hr><br>
		<form action='".base_url()."curlresponse.php' method='POST'>
		Trip Advisor Hotel ID Array<br>
		<input type='text' name='ta_id[]' value='634509'><br>
		<input type='text' name='ta_id[]' value='586362'><br>
		<input type='text' name='ta_id[]' value=''><br> Request by Tripadvisor. <br>
		<small><i>format : array integer</i></small><br><br>
		Start Date<br><input type='text' name='start_date' value='2014-01-15'> Request by Tripadvisor<br>
		<small><i>ex:start date = 2014-01-15  ( format : yyyy-mm-dd )</i></small><br>
		<br>End Date<br><input type='text' name='end_date' value='2014-01-17'> Request by Tripadvisor<br>
		<small><i>ex:end date = 2014-01-17  ( format : yyyy-mm-dd )</i></small><br><br>
		Num Adults<br><input type='text' name='num_adults' value='1'> Request by Tripadvisor<br>
		<small><i>ex:num = 1  ( format : integer )</i></small><br><br>
		Key<br><input type='text' name='query_key' value='cGVnaXBlZ2lrZXlhcGk='> Request by Tripadvisor<br>
		<small><i>ex: cGVnaXBlZ2lrZXlhcGk=  (format : string ) </i></small><br><br>
		<input type='submit' value='send'>
		</form>
		";
    }

    public function _excel2()
    {

        // this is query
        $this->db->select("YAD_NO,YAD_NAME,LOWER(YAD_NAME) as YAD_NAME_ORD,LRG_NAME,YAD_ADRS_ENG,LCL_YBN_NO");
        $this->db->join("USR_JIDSRV01.j_yad_area_map","j_yad_area_map.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->join("USR_JIDSRV01.j_LRG_AREA_OUT_trans","j_LRG_AREA_OUT_trans.LRG_CD=j_yad_area_map.LRG_CD");
        $this->db->join("USR_JIDSRV01.j_yad_ctl","USR_JIDSRV01.j_yad_ctl.YAD_NO=J_YAD_KHN.YAD_NO");
        //$this->db->where("j_yad_ctl.open_flg","1");
        $this->db->order_by('LOWER(YAD_NAME)','ASC');
        //$this->db->limit(2,0);
        $hotels = $this->db->get("USR_JIDSRV01.J_YAD_KHN");

        // echo "<pre>";
        // print_r($hotels);
        // echo "</pre>";
        // die();
        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=hotels_list.xls');
// Fix for crappy IE bug in download.
        header("Pragma: ");
        header("Cache-Control: ");

      
        echo "<table border=1><tr><th>Hotel ID</th>
        <th>Hotel Name</th>
        <th>Kota</th>
        <th>Address</th>
        <th>URL</th>
        <th>Zip Code</th>
        </tr>";

        date_default_timezone_set('Asia/Jakarta');

        foreach($hotels->result() as $val)
        {

        	 $hotel_name_url = str_replace("-","_",url_title(strtolower($val->YAD_NAME)));
             $hotel_id = $val->YAD_NO;
             $lrg_name =  str_replace("-","_",url_title(strtolower($val->LRG_NAME)));

        	$url =  "http://www.pegipegi.com/hotel/".$lrg_name."/".$hotel_name_url."_".$hotel_id."/";

              echo "<tr>
            <td>".$val->YAD_NO."</td>
            <td>".$val->YAD_NAME."</td>
            <td>".$val->LRG_NAME."</td>
            <td>".$val->YAD_ADRS_ENG."</td>
            <td><a href=".$url.">".$url."</a></td>
            <td>".$val->LCL_YBN_NO."</td>
            </tr>";


        }

        echo "<table>";

    }

     protected function _excel()
    {

        // this is query
        $this->db->select("J_YAD_KHN.YAD_NO,J_YAD_KHN.YAD_NAME,j_tripadvisor_hotel.TAID,j_yad_ctl.open_flg");
        $this->db->join("USR_JIDSRV01.j_tripadvisor_hotel","USR_JIDSRV01.j_tripadvisor_hotel.YAD_NO=J_YAD_KHN.YAD_NO","LEFT");
        $this->db->join("USR_JIDSRV01.j_yad_ctl","USR_JIDSRV01.j_yad_ctl.YAD_NO=J_YAD_KHN.YAD_NO");
        //$this->db->where("j_yad_ctl.open_flg","1");
        $this->db->order_by("USR_JIDSRV01.J_YAD_KHN.YAD_NAME","asc");
        $hotels = $this->db->get("USR_JIDSRV01.J_YAD_KHN");

        header('Content-Type: application/force-download');
        header('Content-disposition: attachment; filename=hotels_list.xls');
// Fix for crappy IE bug in download.
        header("Pragma: ");
        header("Cache-Control: ");

        //open
        $this->db->select("J_YAD_KHN.YAD_NO,J_YAD_KHN.YAD_NAME,j_tripadvisor_hotel.TAID,j_yad_ctl.open_flg");
        $this->db->join("USR_JIDSRV01.j_tripadvisor_hotel","USR_JIDSRV01.j_tripadvisor_hotel.YAD_NO=J_YAD_KHN.YAD_NO","LEFT");
        $this->db->join("USR_JIDSRV01.j_yad_ctl","USR_JIDSRV01.j_yad_ctl.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->where("j_yad_ctl.open_flg","1");
        $this->db->order_by("USR_JIDSRV01.J_YAD_KHN.YAD_NAME","asc");
        $open = $this->db->get("USR_JIDSRV01.J_YAD_KHN");

        //close
        $this->db->select("J_YAD_KHN.YAD_NO,J_YAD_KHN.YAD_NAME,j_tripadvisor_hotel.TAID,j_yad_ctl.open_flg");
        $this->db->join("USR_JIDSRV01.j_tripadvisor_hotel","USR_JIDSRV01.j_tripadvisor_hotel.YAD_NO=J_YAD_KHN.YAD_NO","LEFT");
        $this->db->join("USR_JIDSRV01.j_yad_ctl","USR_JIDSRV01.j_yad_ctl.YAD_NO=J_YAD_KHN.YAD_NO");
        $this->db->where("j_yad_ctl.open_flg","0");
        $this->db->order_by("USR_JIDSRV01.J_YAD_KHN.YAD_NAME","asc");
        $close = $this->db->get("USR_JIDSRV01.J_YAD_KHN");


        echo "<b>Total :</b> ".$hotels->num_rows()."<br><b>Open:</b>".$open->num_rows()." <br><b>Closed : ".$close->num_rows()."</b>";

        echo "<table border=1><tr><th>Pegipegi.com Hotel ID</th>
        <th>Pegipegi.com Hotel Name</th>
        <th>Tripadvisor Hotel ID</th>
        <th>Status</th>
        </tr>";

        date_default_timezone_set('Asia/Jakarta');

        foreach($hotels->result() as $val)
        {

            $flag = $val->OPEN_FLG == 1 ? "<b style='color:green'>open</b>" : "<b style='color:red'>closed</b>";

            // echo "<tr>
            // <td>".$val->YAD_NO."</td>
            // <td>".$val->YAD_NAME."</td>
            // <td>".$val->TAID."</td>
            // <td>".$flag."</td>

            // </tr>";


              echo "<tr>
            <td>".$val->YAD_NO."</td>
            <td>".$val->YAD_NAME."</td>
            <td>".$val->TAID."</td>
            <td>".$flag."</td>

            </tr>";


        }

        echo "<table>";

    }

	function readJson()
	{
		$json = base_url()."hotel_api/checkHotelGet/ta_id/212/start_date/2013-09-09/end_date/2013-09-09/num_adults/1";
		$string = file_get_contents($json);
		$json_a=json_decode($string,true);
		//echo $json_a['hotels']['hotel_id'];
		
		echo "<pre>";
		print_r($json_a);
		echo "</pre>";

		// foreach ($json_a as $key) {
		// 	# code...
		// 	echo $key->api_version;
		// }
	}
}
